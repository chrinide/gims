def write_input_files(code_name, data, tmp_dir, species_dir):
    import os.path
    import shutil
    import gims.codes as codes

    # print(code_name+"_control")
    species_dir = os.path.join(species_dir, code_name)
    tar_dir = os.path.join(tmp_dir, "tar")
    input_dir = os.path.join(tar_dir, "input_files")

    code = getattr(codes, code_name)(data, input_dir, species_dir)
    if code.error == None:
        shutil.make_archive(os.path.join(tmp_dir, "input_files"), "tar", tar_dir)
        return tmp_dir, "input_files.tar", code.info
    else:
        return None, "FileNotFoundError", {}


def get_input_files(data_json, species_dir):
    """
    Generate the control.in file from the ASE calculator object.
    species_dir: path to the species root directory.
    """
    import json
    from flask import send_from_directory
    import tempfile

    data = json.loads(data_json)
    # print(data)
    code_name = data["code"]
    with tempfile.TemporaryDirectory() as tmpdirname:

        dir, tar_file, dummy = write_input_files(
            code_name, data, tmpdirname, species_dir
        )

        if tar_file == "FileNotFoundError":
            response = "FileNotFoundError"
        else:
            print("I am sending the tar")
            response = send_from_directory(dir, tar_file, as_attachment=True)

    return response


def get_download_info(data_json, species_dir):
    """
    Generate the control.in file from the ASE calculator object.
    species_dir: path to the species root directory.
    """
    import json
    from flask import Response
    import tempfile

    data = json.loads(data_json)
    # print(data)
    code_name = data["code"]
    with tempfile.TemporaryDirectory() as tmpdirname:

        dir, tar_file, downloadInfo = write_input_files(
            code_name, data, tmpdirname, species_dir
        )

        if tar_file == "FileNotFoundError":
            response = "FileNotFoundError"
        else:
            print("I am sending the tar")
            response = json.dumps(downloadInfo)
            # response = Response(downloadInfoJSON, mimetype="application/json")

    return response


def get_structure_info(structure, symThresh):
    from gims.structure_info import Structure_info

    s_info = Structure_info(structure, symThresh)
    return s_info.get_info()


def primtive_cell(data_json):
    import spglib as spg
    import json
    from gims.codes import Code
    from gims.structure_info import Structure_info

    data = json.loads(data_json)
    # print(data)
    symThresh = float(data["symThresh"])
    atoms = Code.atomsFromDict(data)
    si = Structure_info(atoms, symThresh)
    primitive = si.primitive
    f_name = data["fileName"] + " (primitive)"
    # print(primitive)
    if len(primitive) == len(atoms):
        response = "ErrorIsAlreadyPrimitive"
    else:
        response = write_json_from_atoms(primitive, f_name, symThresh)

    return response


def write_json_from_atoms(struct, fileName, symThresh=1e-5):
    """
    Prepares the json file from an ASE atoms object 'struct'.
    """
    import json
    import numpy as np
    import os

    json_s = {"lattice": [], "atoms": [], "fileName": os.path.basename(fileName)}

    json_s["structureInfo"] = get_structure_info(struct, symThresh)

    # print(json_s["structureInfo"])
    pbc = np.any(struct.pbc)
    if pbc:
        for v in struct.cell.array:
            json_s["lattice"].append(list(v))
    else:
        json_s["lattice"] = None
    atomsData = zip(
        struct.get_positions(),
        struct.get_chemical_symbols(),
        struct.get_initial_magnetic_moments(),
    )
    # print(struct.constraints[0].index)
    for i, (pos, species, initMoment) in enumerate(atomsData):
        # Tried to bring it in the correct form for this function:
        # structure.addAtomData(atomPos, tokens[4], tokens[0] === ATOM_FRAC_KEYWORD)
        # in src/common/util.js. The last entry is always false, since we obtain
        # cartesian positions from ase.
        constraint = False
        try:
            if i in struct.constraints[0].index:
                constraint = True
                print([list(pos), species, "", constraint])
        except:
            pass
        json_s["atoms"].append([list(pos), species, "", initMoment, constraint])
    return json.dumps(json_s)


def structure_auto_detection(file_name):
    from ase.io import read

    try:
        # The format is specified by the file extension.
        structure = read(file_name)
        # print('struct', structure)
        assert len(structure.get_positions()) > 0
    except KeyError:
        # Could not guess the proper file format.
        return "Could not properly read the structure file"
    except:
        return "ErrorParsingGeometryFile"
        # print('Something else went wrong. Please excuse!')
    else:
        json_structure = write_json_from_atoms(structure, file_name)
    return json_structure


def get_json_structure(files):
    """
    Read the uploaded structure file and prepare json file, which the client
    understands.
    """
    import os.path
    import tempfile
    from werkzeug.utils import secure_filename

    f = files["file"]
    with tempfile.TemporaryDirectory() as tmpdirname:
        save_name = os.path.join(tmpdirname, secure_filename(f.filename))
        f.save(save_name)
        return structure_auto_detection(save_name)


def update_json_structure(data_json):
    import json
    from gims.codes import Code

    json_structure = json.dumps("")
    try:
        data = json.loads(data_json)
        # print(data)
        atoms = Code.atomsFromDict(data)
        symThresh = float(data["symThresh"])
        json_structure = write_json_from_atoms(atoms, "", symThresh=symThresh)
    except:
        pass
    return json_structure
