from gims.prepare_input import update_json_structure
import json

json_byte = b'{"cell":[[7.55,0,0],[0,7.55,0],[0,0,7.55]],"positions":[{"position":[0,3.775,3.775],"species":"Cd","initMoment":0,"constraint":false},{"position":[5.6625,5.6625,1.8875],"species":"Sr","initMoment":0,"constraint":false},{"position":[3.775,3.775,3.775],"species":"Ta","initMoment":0,"constraint":false},{"position":[0,0,0],"species":"Cd","initMoment":0,"constraint":false},{"position":[5.6625,1.8875,5.6625],"species":"Sr","initMoment":0,"constraint":false},{"position":[3.775,0,0],"species":"Ta","initMoment":0,"constraint":false},{"position":[3.775,3.775,0],"species":"Cd","initMoment":0,"constraint":false},{"position":[1.8875,5.6625,5.6625],"species":"Sr","initMoment":0,"constraint":false},{"position":[0,3.775,0],"species":"Ta","initMoment":0,"constraint":false},{"position":[3.775,0,3.775],"species":"Cd","initMoment":0,"constraint":false},{"position":[1.8875,1.8875,1.8875],"species":"Sr","initMoment":0,"constraint":false},{"position":[0,0,3.775],"species":"Ta","initMoment":0,"constraint":false}],"symThresh":0.00001}'

expected_answer = {
    "lattice": [[7.55, 0.0, 0.0], [0.0, 7.55, 0.0], [0.0, 0.0, 7.55]],
    "atoms": [
        [[0.0, 3.775, 3.775], "Cd", "", 0.0, False],
        [[5.6625, 5.6625, 1.8875], "Sr", "", 0.0, False],
        [[3.775, 3.775, 3.775], "Ta", "", 0.0, False],
        [[0.0, 0.0, 0.0], "Cd", "", 0.0, False],
        [[5.6625, 1.8875, 5.6625], "Sr", "", 0.0, False],
        [[3.775, 0.0, 0.0], "Ta", "", 0.0, False],
        [[3.775, 3.775, 0.0], "Cd", "", 0.0, False],
        [[1.8875, 5.6625, 5.6625], "Sr", "", 0.0, False],
        [[0.0, 3.775, 0.0], "Ta", "", 0.0, False],
        [[3.775, 0.0, 3.775], "Cd", "", 0.0, False],
        [[1.8875, 1.8875, 1.8875], "Sr", "", 0.0, False],
        [[0.0, 0.0, 3.775], "Ta", "", 0.0, False],
    ],
    "fileName": "",
    "structureInfo": {
        "n_atoms": {"value": 12, "info_str": "Number of atoms"},
        "formula": {"value": "Cd4Sr4Ta4", "info_str": "Chemical formula"},
        "sym_thresh": {"value": 1e-05, "info_str": "Symmetry Threshold"},
        "bravais": {
            "value": "primitive cubic CUB(a=7.55)",
            "info_str": "Bravais Lattice",
        },
        "unit_cell_parameters": {
            "value": [7.55, 7.55, 7.55, 90.0, 90.0, 90.0],
            "info_str": "Lattice parameters <br> (a, b, c, α, β, γ)",
        },
        "spacegroup": {"value": 216, "info_str": "Spacegroup number"},
        "hall_symbol": {"value": "F -4 2 3", "info_str": "Hall symbol"},
        "occupied_wyckoffs": {
            "value": ["a", "b", "c"],
            "info_str": "Occupied Wyckoff positions",
        },
        "equivalent_atoms": {"value": [1, 2, 3], "info_str": "Unique equivalent atoms"},
        "is_primitive": {"value": False, "info_str": "Is primitive cell?"},
    },
}


class TestupdateStructureInfo:

    def test_updateStructureInfo(self):
        answer = json.loads(update_json_structure(json_byte))
        for key, value in answer.items():
            assert expected_answer[key] == value
