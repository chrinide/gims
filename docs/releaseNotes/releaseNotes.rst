Release Notes
===============

The following threads will keep you updated on the upcoming GIMS releases.


Release Plans
-------------

This section announces the next planned releases and updates of the running GIMS servers.

October 6, 2020
+++++++++++++++

Release of version 1.0.0 and simultaneous update of the servers https://gims.ms1p.org and https://gims-dev.ms1p.org.
For any future patch or minor update, only the gims-dev.ms1p.org server will be updated first.
Only an official release will trigger an update of the main server gims.ms1p.org.

Changelog
---------

The section *Changelog* describes and documents the implementation of new features, fixes and so on, of all releases.

.. toctree::

    changelog
