
Where to find GIMS
==================

Running Versions
----------------

Right now we maintain two servers running the GIMS application.

1. Stable Version
    The latest stable version runs on https://gims.ms1p.org. This server runs version ``1.0.1`` of GIMS.

2. Development Version
    We also try to offer to the user the latest version from the master branch. However, there is no automatic deployment, so there might be some delay involved. You can find the development version right here: https://gims-dev.ms1p.org. Currently, this server runs version ``1.0.1`` of GIMS.

GitLab
------

The GIMS project is hosted on Gitlab: https://gitlab.com/gims-developers/gims.
Feel free to join the group of developers and contribute your own features to GIMS.
