
About
=====

The objective of *GIMS* is to lower the entry barrier and to provide an easy-to-use, platform-independent, zero-setup toolbox for standard tasks within the framework of first-principles electronic-structure calculations. Information about running GIMS application can be found here: `Where to find GIMS <whereToFind.html>`_. GIMS is intentionally written and designed to be easily extendable to any electronic-structure code. At present, it supports the ``FHI-aims`` and ``exciting`` codes.

Feature List
------------

* Structure visualisation of various different formats: investigate, manipulate, export, and take snapshots of your structure.
* Select your needed input parameters from a list, which includes detailed explanations for all keywords.
* Visualize all of your output files (main output files, density of states, band structure + Brillouin Zone and Path) with ease. Publication ready graphs and pictures can be downloaded.
* Select specific workflows. The workflow will guide you through the needed steps and background checks will help to avoid easy setup errors for your calculation. 
