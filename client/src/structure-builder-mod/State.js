/**
 * @author Iker Hurtado
 *
 * @fileoverview File holding the module State class
 */

import {generateSupercell} from '../common/util.js'
import {Conf} from '../Conf.js'
import {CHANGE} from '../State.js'


/**
 * Structure Builder state and implements the undo/redo functionality
 */
export default class State{

  constructor(){

    // Fractional state and listener
    this.fractional = false //  fractional coordinates
    this.fractionalListeners = []

    // Structure state and listeners
    this.structure
    this.structureIDcounter = -1
    this.structureID = -1
    this.structureStates = {}

    this.structureListeners = []
    this.atomHighlightListeners = []
    this.speciesColorListeners = []

    // Undo/redo system
    this.changeList = []

    this.undoIndex = -1 // Points to the last element
    this.undoRedoListener
  }


  // Fractional state - Atoms data can be shown in fractional or cartesian coordinates

  /**
   * Returns the if the view is in fractional (true) or cartesian (false)
   * @return {boolean}
   */
  getFractional(){
    return this.fractional
  }


  /**
   * Sets the fractional state and emits the corresponding event
   * @param {boolean}
   */
  setFractional(fract){
    this.fractional = fract
    this.fractionalListeners.forEach( component => {
      // console.log('component',component)
      component.updateFractional(this.fractional)
    })
  }


  // Structure state - Data of the current structure being shown

  /**
   * Returns the current structure
   * @return {Structure}
   */
  getStructure(){
    return this.structure
  }


  /**
   * Returns a structure atom position in fractional or cartesian
   * coordinates depending on the fractional state
   * @param  {int} i Atom index in the structure
   * @return {Array<float>}
   */
  getAtomPosition(i){
    return ( this.fractional ?
      this.structure.getAtomFractPosition(i) :
      this.structure.atoms[i].position )
  }


  /**
   * Sets a new structure in the state and a new structure event is emitted.
   * The undo/redo is reseted (default)
   * @param {Structure} s
   * @param {boolean} undoRedoReset
   */
  setStructure(s, undoRedoReset = true){
    this.saveOldState()
    this.structure = s
    this.structureIDcounter += 1
    this.structure.id = this.structureIDcounter
    this.structureID = this.structureIDcounter
    this.structureStates[this.structureID] = {
      'structure': this.structure,
      'undoIndex': this.undoIndex,
      'changeList': this.changeList
    }
    if (undoRedoReset) this.resetUndoRedo() // regular behavior

    if (!this.structure.isAPeriodicSystem()) this.setFractional(false)

    this.emitNewStructure()
    // console.log('this.structureStates',this.structureStates)
  }

  saveOldState() {
    // console.log('saveOldState:ID',this.structureID)
    if (this.structureID >= 0) {
      this.structureStates[this.structureID] = {
        'structure': this.structure,
        'undoIndex': this.undoIndex,
        'changeList': this.changeList
      }
    }
  }

  selectStructure(structureID,save=true) {
    if(save) this.saveOldState()
    let selected = this.structureStates[structureID]
    // console.log('Huhu', this.structureStates)
    this.structure = selected.structure
    this.undoIndex = selected.undoIndex
    this.changeList =  selected.changeList
    this.structureID = structureID
    this.emitNewSelection()
  }


  /**
   * Resets the state structure and a new structure event is emitted.
   */
  resetStructure(){
    this.structure.reset()
    this.structure.fileSource = 'New Structure'
    this.resetUndoRedo()
    if (!this.structure.isAPeriodicSystem()) this.setFractional(false)
    this.emitNewStructure()
  }


  /**
   * Transforms the structure in a supercell of it.
   * A new structure event is emitted
   * @param  {Array<float>} dimArray Array specifying the multiplication of the axes
   * @param  {boolean} setChange If the change (undo/redo) will be registered
   * @return {boolean} Returns true if everything was ok
   */
  repeatStructure(dimArray, setChange = false){  // multiply? structure
    let supercell = generateSupercell(dimArray, this.structure)
    if (supercell.atoms.length < 5000){
      if (setChange)
        this.addChange(CHANGE.STRUCTURE.SUPERCELL, {update: [this.structure, supercell]})
      this.setStructure(supercell, true)
      return true
    }
    else return false
  }


  /**
   * Updates the structure lattice
   * A structure change event is emitted
   * @param  {Array<Array<float>>} vectors
   * @param  {boolean} scaleAtomsPosition
   *   If true the atoms positions will scale with the new lattice vectors
   * @param  {boolean} setChange If the change (undo/redo) will be registered
   */
  updateLatticeVectors(vectors, scaleAtomsPosition = false, setChange = true){
    // vectors === undefined means  lattice vectors are removed from the structure
    if (setChange){
      this.addChange(CHANGE.STRUCTURE.LATTICE_VECTORS,
        {update: [this.structure.latVectors, vectors, scaleAtomsPosition]})
    }

    this.setFractional(false)
    if (vectors === undefined){ // remove lattice vectors
      this.structure.removeLatticeVectors()
    }else
      this.structure.updateLatticeVectors(vectors, scaleAtomsPosition, true)
    //console.log('updateLatticeVectors', structure)
    this.emitStructureChange({type: CHANGE.STRUCTURE.LATTICE_VECTORS})
  }


  /**
   * Updates an atom species. A structure change event is emitted
   * @param  {int} atomIndex
   * @param  {string} species
   * @param  {boolean} setChange If the change (undo/redo) will be registered
   */
  updateAtomSpecies(atomIndex, species, setChange = true){
    if (setChange){
      let update = [this.structure.atoms[atomIndex].species, species]
      this.addChange(CHANGE.STRUCTURE.ATOM.SPECIES, {atomId: atomIndex, update: update})
    }
    this.structure.updateAtomSpecies(atomIndex, species)
    this.emitStructureChange({type: CHANGE.STRUCTURE.ATOM.SPECIES, atomIndex: atomIndex})//['atom-update', atomIndex])
  }


  /**
   * Updates an atom initial moment. A structure change event is emitted
   * @param  {int} atomIndex
   * @param  {float} initMoment
   * @param  {boolean} setChange If the change (undo/redo) will be registered
   */
  updateAtomInitMoment(atomIndex, initMoment, setChange = true){
    if (setChange){
      let update = [this.structure.atoms[atomIndex].initMoment, initMoment]
      this.addChange(CHANGE.STRUCTURE.ATOM.INITMOMENT, {atomId: atomIndex, update: update})
    }
    this.structure.updateAtomInitMoment(atomIndex, initMoment)
    this.emitStructureChange({type: CHANGE.STRUCTURE.ATOM.INITMOMENT, atomIndex: atomIndex})//['atom-update', atomIndex])
  }


  /**
   * Updates an atom constraint. A structure change event is emitted
   * @param  {int} atomIndex
   * @param  {boolean} constraint
   * @param  {boolean} setChange If the change (undo/redo) will be registered
   */
  updateAtomConstraint(atomIndex, constraint, setChange = true){
    if (setChange){
      let update = [this.structure.atoms[atomIndex].constraint, constraint]
      this.addChange(CHANGE.STRUCTURE.ATOM.CONSTRAINT, {atomId: atomIndex, update: update})
    }
    console.log('State.updateAtomConstraint',constraint)
    this.structure.updateAtomConstraint(atomIndex, constraint)
    this.emitStructureChange({type: CHANGE.STRUCTURE.ATOM.CONSTRAINT, atomIndex: atomIndex})//['atom-update', atomIndex])
  }


  /**
   * Updates an atom position. A structure change event is emitted
   * @param  {int} atomIndex
   * @param  {Array<float>} values
   * @param  {boolean} fract If the position is specified in
   *   fractional coordenates (true) or cartesian (false)
   */
  updateAtomPosition(atomIndex, values, fract){
    const f = (fract === undefined ? this.fractional : fract)
    // change addition
    let newPosition = (f ? this.structure.getCartesianCoordinates(values) : values)
    // Using slice() in order to generate anew array, not a reference
    let update = [this.structure.atoms[atomIndex].position.slice(), newPosition]
    this.addChange(CHANGE.STRUCTURE.ATOM.MOVE, {atomId: atomIndex, update: update})

    this.structure.updateAtomPosition(atomIndex, newPosition)
    this.emitStructureChange({type: CHANGE.STRUCTURE.ATOM.MOVE, atomIndex: atomIndex})//['atom-update', atomIndex])
  }


  /**
   * Updates an atom position but the change (undo/redo) will not be registered.
   * A structure change event is emitted
   * @param  {int} atomIndex
   * @param  {Array<float>} values
   */
  updateAtomPositionNoChange(atomIndex, values){
    this.structure.updateAtomPosition(atomIndex, values, false)
    this.emitStructureChange({type: CHANGE.STRUCTURE.ATOM.MOVE, atomIndex: atomIndex})
  }


  /**
   * Adds an undefiend atom to the structure.
   * A structure change event is emitted
   * @param  {boolean} setChange If the change (undo/redo) will be registered
   */
  addUndefinedAtom(setChange = true){
    const atomIndex = this.structure.addUndefinedAtom()
    if (setChange) this.addChange(CHANGE.STRUCTURE.ATOM.ADD, {atomId: atomIndex})
    //console.log('addUndefinedAtom', structure)
    this.emitStructureChange({type: CHANGE.STRUCTURE.ATOM.ADD, atomIndex: atomIndex})//['atom-addition', atomIndex])
  }


  /**
   * Removes and atom from the structure. A structure change event is emitted
   * @param  {int} atomIndex
   * @param  {boolean} setChange If the change (undo/redo) will be registered
   */
  removeAtom(atomIndex, setChange = true){
    if (setChange){
      // get data from the atom to remove !!!  structure.atoms[atomIndex]
      this.addChange(CHANGE.STRUCTURE.ATOM.REMOVE,
        {atomId: atomIndex, removedAtom: this.structure.atoms[atomIndex]})
    }
    this.structure.atoms.splice(atomIndex, 1)
    this.emitStructureChange(
      {type: CHANGE.STRUCTURE.ATOM.REMOVE, atomIndex: atomIndex})
  }



  // Species color change - The species showing color can be changed

  /**
   * Changes the color of a species. A species color change event is emitted
   * @param  {string} species
   * @param  {string} color Color code
   */
  changeSpeciesColor(species, color){
    // Update the species color in Conf.js
    Conf.setSpeciesColor(species, color)
    //console.log('changeSpeciesColor', species, color)
    this.speciesColorListeners.forEach( component => {
      component.changeSpeciesColor(species, color)
    })
  }



  // Subcriptions and event emission


  /**
   * Subscribes a component to the main module events:
   * structure, fractional and species color
   * @param  {Component} component
   */
  subscribeToAll( component ){
    this.structureListeners.push(component)
    this.fractionalListeners.push(component)
    this.speciesColorListeners.push(component)
  }


  /**
   * Subscribes a component to the structure events:
   * @param  {Component} component
   */
  subscribeToStructure( component ){
    this.structureListeners.push(component)
  }


  /**
   * Subscribes a component to the species color event:
   * @param  {Component} component
   */
  subscribeToSpeciesColorChange( component ){
    this.speciesColorListeners.push(component)
  }


  /**
   * Emits a structure change event
   * @param  {object} change
   */
  emitStructureChange(change){
    // console.log('emitStructureChange', change)
    this.structureListeners.forEach( component => {
      component.updateStructure(this.structure, change )
    })

  }


  /**
   * Emits a new structure event
   */
  emitNewStructure(){
    this.structureListeners.forEach( component => {
      // console.log('emitNewStructure',  component, component.constructor.name)
      component.setNewStructure(this.structure)
    })

  }

  emitNewSelection(){
    this.structureListeners.forEach( component => {
      // console.log('emitNewSelection',  component, this.structure)
      component.selectStructure(this.structure)

    })
  }


  /**
   * Subscribes a component to the atom highlight event:
   * @param  {Component} component
   */
  subscribeToAtomHighlight( component ){
    this.atomHighlightListeners.push(component)
  }


  /**
   * Commands an atom highlight event
   * @param  {object} atomIndex
   * @param  {object} on
   */
  highlightAtom(atomIndex, on = true){
    this.emitHighlightAtom(atomIndex, on)
  }


  /**
   * Emits an atom highlight event
   * @param  {object} atomIndex
   * @param  {boolean} on
   */
  emitHighlightAtom(atomIndex, on){
    this.atomHighlightListeners.forEach( component => {
      //console.log('emitStructureChange ',  component)
      component.highlightAtom(atomIndex, on)
    })
  }


  // Undo/redo functionality section

  /**
   * Adds a change to the undo/redo list and move the pointer
   * @param {string} type change type
   * @param {object} detail Change additional information
   */
  addChange(type, detail){
    // Check if the index is not pointing to the last change
    if (this.undoIndex < this.changeList.length-1){
      this.changeList.length = this.undoIndex+1
    }
    this.changeList.push( {type: type, detail: detail})
    this.undoIndex++ // Pointing to the last element
    this.undoRedoListener(this.undoIndex+1, this.changeList.length)
    //console.log('addChange:', this.changeList, this.undoIndex)
  }


  /**
   * Resets the undo/redo system
   */
  resetUndoRedo(){
    this.changeList = []
    this.undoIndex = -1
    this.undoRedoListener(this.undoIndex+1, this.changeList.length)
  }


  /**
   * Performs an undo operation: update the undo/redo list
   * and carries out the appropriate operation on the structure state
   */
  undo(){
    //console.log('this.undoIndex:', this.undoIndex)
    if (this.undoIndex < 0) return

    let change = this.changeList[this.undoIndex]
    switch (change.type) {

      case CHANGE.STRUCTURE.SUPERCELL:
        //let reducedStructure = util.reverseSupercell(change.detail.update, structure)
        this.setStructure(change.detail.update[0], false)
        break;

      case CHANGE.STRUCTURE.LATTICE_VECTORS:
        this.updateLatticeVectors(change.detail.update[0], change.detail.update[2], false)
        break;

      case CHANGE.STRUCTURE.ATOM.SPECIES:
        this.updateAtomSpecies(change.detail.atomId, change.detail.update[0], false)
        break;

      case CHANGE.STRUCTURE.ATOM.INITMOMENT:
        this.updateAtomInitMoment(change.detail.atomId, change.detail.update[0], false)
        break;

      case CHANGE.STRUCTURE.ATOM.CONSTRAINT:
        this.updateAtomConstraint(change.detail.atomId, change.detail.update[0], false)
        break;

      case CHANGE.STRUCTURE.ATOM.MOVE: //ATOM.MOVE:
        this.updateAtomPositionNoChange(change.detail.atomId, change.detail.update[0])
        break;

      case CHANGE.STRUCTURE.ATOM.ADD:
        this.removeAtom(change.detail.atomId, false)
        break;

      case CHANGE.STRUCTURE.ATOM.REMOVE:
        this.structure.atoms.splice(change.detail.atomId, 0, change.detail.removedAtom)
        this.emitStructureChange({type: CHANGE.STRUCTURE.ATOM.ADD, atomIndex: change.detail.atomId})
        break;


    }
    this.undoIndex--
    this.undoRedoListener(this.undoIndex+1, this.changeList.length)
  }


  /**
   * Performs a redo operation: update the undo/redo list
   * and carries out the appropriate operation on the structure state
   */
  redo(){
    //console.log('redoIndex:', this.undoIndex)
    if (this.undoIndex === this.changeList.length-1) return
    else{
      this.undoIndex++
      this.undoRedoListener(this.undoIndex+1, this.changeList.length)
    }
    let change = this.changeList[this.undoIndex]
    switch (change.type) {

      case CHANGE.STRUCTURE.SUPERCELL:
        //repeatStructure(change.detail.update, false)
        this.setStructure(change.detail.update[1], false)
        break;

      case CHANGE.STRUCTURE.ATOM.SPECIES:
        this.updateAtomSpecies(change.detail.atomId, change.detail.update[1], false)
        break;

      case CHANGE.STRUCTURE.ATOM.INITMOMENT:
        this.updateAtomInitMoment(change.detail.atomId, change.detail.update[1], false)
        break;

      case CHANGE.STRUCTURE.ATOM.CONSTRAINT:
        this.updateAtomConstraint(change.detail.atomId, change.detail.update[1], false)
        break;

      case CHANGE.STRUCTURE.ATOM.MOVE:
        this.updateAtomPositionNoChange(change.detail.atomId, change.detail.update[1])
        break;

      case CHANGE.STRUCTURE.ATOM.ADD:
        //removeAtom(change.detail.atomId, false)
        this.addUndefinedAtom(false)
        break;

      case CHANGE.STRUCTURE.ATOM.REMOVE:
        this.removeAtom(change.detail.atomId, false)
        break;

      case CHANGE.STRUCTURE.LATTICE_VECTORS:
        this.updateLatticeVectors(change.detail.update[1], change.detail.update[2], false)
        break;
    }
  }


  /**
   * Sets the listener that will be called when a undo/redo operation
   * is carried out
   * @param {function} listener
   */
  setUndoRedoListener(listener){
    this.undoRedoListener = listener
  }

}
