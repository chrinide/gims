/**
* @author Sebastian Kokott
*
* @fileoverview File holding the Structure Info UI component
*/

import UIComponent from '../common/UIComponent.js'
import * as UserMsgBox from '../common/UserMsgBox.js'
import {Conf} from '../Conf.js'

export default class StructureInfo extends UIComponent{
  constructor(moduleState) {
    super('span', '.SupercellField')

    this.moduleState = moduleState // We might need this to update the info panel later on...

    this.setHTML(`
      <div class="addon-box">
      <div class="addon" >Structure Info</div>
        <div class="addon-content StructureInfo" style="display:none">
          <table></table>
          <button>Update Structure Info</button>
        </div>
      </div>
      </div>
    `)

    let siButton = this.getElement('.addon')
    siButton.addEventListener('click',() => {
      let content = this.getElement('.addon-content')
      if (content.style.display === 'none') {
        content.style.display	= 'inline-block'
      } else {
        content.style.display = 'none'
      }
    })
    let button = this.getElement('button')
    let updateStructureInfoHandler = () => {
      this.updateStructureInfo()
    }
    button.addEventListener('click',updateStructureInfoHandler)
  }

  setNewStructure(structure){
    if (structure.structureInfo === undefined){
      structure.structureInfo = {}
    }
    this.updateStructureInfo()
  }

  selectStructure(structure){
    this.updateStructureInfo()
  }

  updateTable(structureInfo){
    let table = this.getElement('table')
    table.innerHTML = ''
    if (structureInfo !== undefined) {
      for (const [key, value] of Object.entries(structureInfo)) {
        // console.log(key, value);
        let tr = document.createElement("tr")
        let td1 = document.createElement("td")
        td1.innerHTML = value.info_str
        let td2 = document.createElement("td")
        if (Array.isArray(value.value)) {
          td2.innerHTML = value.value.join(', ')
        } else
          td2.innerHTML = value.value
        tr.appendChild(td1)
        tr.appendChild(td2)
        table.appendChild(tr)
      }
    }

  }

  updateStructure(){
    // We don't wanna update the structure info every time there is a structure change.
    // It can be requested with the button at the bottom.
  }

  async updateStructureInfo(){
    let si = {}
    let structure = this.moduleState.getStructure()
    // console.log(structure, !structure.atoms.length)
    if (structure.atoms.length) {
      let sendStructure = { cell: structure.latVectors, positions: structure.atoms, symThresh: Conf.settings.symmetryThreshold}
      let response = await fetch('/update-structure-info', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json;charset=utf-8' },
        body: JSON.stringify(sendStructure)
      })

      if (!response.ok){
        UserMsgBox.setError('Unknown server ERROR ')
        return
      }

      const text = await response.text()
      let structureData = JSON.parse(text)
      si = structureData ? structureData.structureInfo : undefined

    }

    // console.log(structureData)
    this.updateTable(si)
  }

  updateSettings() {
    this.updateStructureInfo()
  }

}
