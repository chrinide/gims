/**
 * @author Iker Hurtado
 *
 * @fileoverview File holding the BasisAtoms UI component
 */


import {Conf} from '../Conf.js'
import UIComponent from '../common/UIComponent.js'
import CoordinatesField from './CoordinatesField.js'
import * as State from './State.js'
import binIcon from '../../img/binicon.png'
import prevArrowIcon from '../../img/prev-arrow-white-icon.png'
import nextArrowIcon from '../../img/next-arrow-white-icon.png'

const ROW_NUMBER = 10


/**
 * A UI panel showing the list of atoms comprising a structure
 */
export default class BasisAtoms extends UIComponent{

	/**
	 * @param  {State} moduleState
	 *   Object representing the state of the Structure builder module
	 */
	constructor(moduleState){

		super('div', '#BasisAtoms')

		// component state
		this.page = 1
		this.pagesNum = 1
		this.moduleState = moduleState

    	this.setHTML(`
    		<div class="h2-title">BASIS ATOMS</div>

    		<div id="atom-selector">
    			<button class="prev-page">
    			  <img src="${prevArrowIcon}" height="16px" style="vertical-align: middle;">
    			</button>
    			<span class="current-page-label" style="font-size: 1.2em; vertical-align: middle;"></span>
    			<button class="next-page">
    			  <img src="${nextArrowIcon}" height="16px" style="vertical-align: middle;">
    			</button>
    			<!--<button class="next-page"> &gt; </button> -->
    		</div>

    		<div id="NewRowButton">
    			<span id="coordinates-switch">
    				<input type="checkbox" >Fractional coordinates
    			</span>
    			<button>New atom</button>
    		</div>
				<div class="units-texts units-basis-atoms" style="margin-left:80px; font-size:14px;font-weight:lighter"><i>All numbers in units of &#197;</i></div>

    		<div class="AtomsBlock"></div>
    	`)

    	const prevButton = this.getElement('button.prev-page')
    	const nextButton = this.getElement('button.next-page')
    	prevButton.addEventListener( 'click', e => {
    		if (this.page > 1){
    			this.page--
    			this.update()
    		}
    	})
    	nextButton.addEventListener( 'click', e => {
    		let atoms = this.moduleState.getStructure().atoms
    		if (atoms.length >= this.page*10+1){
    			this.page++
    			this.update()
    		}
    	})

    	this.currentPageLabel = this.getElement('.current-page-label')
			this.units = this.getElement('.units-basis-atoms')
    	this.fractCheckbox = this.getElement('#coordinates-switch input')
    	this.fractCheckbox.addEventListener('click', e => {
    		this.moduleState.setFractional(e.target.checked)
				this.units.style.display = e.target.checked ? "none" : "block"
    	})

    	this.button = this.getElement('#NewRowButton button')
    	this.button.addEventListener('click', e => {
    		this.moduleState.addUndefinedAtom()
    		this.page = this.pagesNum
    		this.update()
    	})

    	this.atomRows = []
    	this.tooltip = new AtomRowTooltip(this.moduleState) // hidden initially
	    this.tooltip.e.style.visibility = 'hidden'

    	const atomsBlockEl = this.getElement('.AtomsBlock')
	    for (let i = 0; i < ROW_NUMBER; i++) {
	    	const row = new AtomRow(this.tooltip, this.moduleState)
	    	this.atomRows.push(row)
	      	atomsBlockEl.appendChild(row.e)
	      	row.e.addEventListener('mouseenter', e => {
	    			this.moduleState.highlightAtom((this.page-1)*ROW_NUMBER+i)
	    		})
	      	row.e.addEventListener('mouseleave', e => {
	    			this.moduleState.highlightAtom((this.page-1)*ROW_NUMBER+i, false)
	    		})
	    }
	}


	 /**
	 * Updates the component to a new structure
	 * Listener for the 'new structure' module level event
	 * @param {Structure} structure
	 */
	 setNewStructure(structure){
	    this.page = 1
	    this.updateStructure(structure)
  	}

		selectStructure(structure){
	    this.setNewStructure(structure)
	  }

    /**
	 * Updates the component to a structure change
	 * Listener for the 'update structure' module level event
	 * @param {Structure} structure
	 * @param {object} change
	 */
  	updateStructure(structure, change){
  		//console.log('BasisAtoms.updateStructure', change)
	    let atoms = this.moduleState.getStructure().atoms

	    this.pagesNum = Math.ceil(atoms.length/ROW_NUMBER)

	    // When the first atmos is created the page has to be set to 1
	    if (this.pagesNum > 0 && this.page === 0 ) this.page = 1
	    // keep the this.page state if it's not bigger than the page number (the number of atoms could change)
	    if (this.page > this.pagesNum) this.page = this.pagesNum

	    this.update()
	 }


    /**
	 * Updates the component to a fractional change in the module
	 * Listener for the 'update fractional' module level event
	 * @param {boolean} fract
	 */
  	updateFractional(fract){
  		this.update()
  	}


  	/**
	 * Updates the component to a atom species change
	 * Listener for the 'change species' module level event
	 * @param {string} species
	 * @param {string} color Color code
	 */
  	changeSpeciesColor(species, color){
  		this.update()
  	}

  	/**
  	 * Updates the component looking at the data model
  	 */
  	update(){

  		this.getElement('#atom-selector').style.display = (this.pagesNum > 1 ? 'block' : 'none')
  		this.currentPageLabel.innerHTML = this.page+'/'+this.pagesNum

  		const periodic = this.moduleState.getStructure().isAPeriodicSystem()
  		this.getElement('#coordinates-switch').style.display = (periodic ? 'inline': 'none')

  		this.fractCheckbox.checked = this.moduleState.getFractional()

  		let atoms = this.moduleState.getStructure().atoms

  		if (atoms.length === 0){ // exceptional case
  			for (let i = 0; i < ROW_NUMBER; i++)
	    		this.atomRows[i].e.style.display = 'none'
	    	return
  		}

	    const initIndex = (this.page-1)*ROW_NUMBER

  		for (let i = 0; i < ROW_NUMBER; i++) {
	    	let atomsIndex = initIndex+i
	    	if (atomsIndex < atoms.length){
	    		this.atomRows[i].setValues(atomsIndex, atoms[atomsIndex])
	    		this.atomRows[i].e.style.display = 'flex'//visibility = 'visible'
	    	}else this.atomRows[i].e.style.display = 'none'//visibility = 'hidden'
	    }
  	}

}



/**
 * Tooltip showing extra info about the atom. It's editable.
 * The atom constraint and initial moment can be changed
 */
class AtomRowTooltip extends UIComponent{

  /**
	* @param  {State} moduleState
	*   Object representing the state of the Structure builder module
	*/
  constructor(moduleState){

    super('div', '.AtomRowTooltip')

    this.setHTML(`<table>
      <tr> <td style="text-align: center"><input type="checkbox" class="constraint" /> </td><td>Constrain Atom</td></tr>
      <tr> <td style="text-align: center"> <input type="text" class="init-moment" /></td><td>Initial Moment</td></tr>
      <tr> <td class="close-button" colspan="2" style="text-align: center; font-size: smaller"> CLOSE</td></tr>
	  </table>
    `)

    this.moduleState = moduleState

    this.constraintCB = this.getElement('.constraint')
    this.constraintCB.addEventListener('change', e => {
      //console.log('constraint',constraint)
      this.moduleState.updateAtomConstraint(this.atomIndex, this.constraintCB.checked)
    })

    this.initMomentField = this.getElement('.init-moment')
    this.initMomentField.addEventListener('change', e => {
      let initMoment = (this.initMomentField.value === '' ? undefined : this.initMomentField.value)
      this.moduleState.updateAtomInitMoment(this.atomIndex, initMoment)
    })

	this.getElement('.close-button').addEventListener('click', e => {
	  this.e.style.visibility = 'hidden'
	})
  }


  /**
   * Sets the atom index in the structure in order to
   * show/edit it
   * @param {int} index
   */
  setAtomIndex(index){
  	this.atomIndex = index
  }


  /**
   * Returns true if the either the constraint or the init moment are filled
   * @return {boolean}
   */
  isMomentOrConstraintSet(){
  	return (this.initMomentField.value !== '' || this.constraintCB.checked)
  }


  /**
   * Resets the atom constraint and init moment
   * @param  {string} moment
   * @param  {boolean} constraint
   */
  reset(moment = '', constraint = false){
  	this.constraintCB.checked = constraint
  	this.initMomentField.value = moment
  }

}



/**
 * Editable atom row
 */
class AtomRow extends UIComponent{

	/**
	 * @param {AtomRowTooltip} tooltip Row tooltip component
	 * @param  {State} moduleState
	 *   Object representing the state of the Structure builder module
	 */
	constructor(tooltip, moduleState){
    	super('div', '.AtomRow')
	    this.setHTML(`
	      <span class="atom-order"> </span>
	      <span class="coordinates-wrapper"></span>
	      <input type="text" class="species-value" />
		  <span style="position: relative" class="species-circle">
		  	<canvas width="18" height="19" ></canvas>
		  </span>
		  <img class="img-button" src="${binIcon}">
	    `)

	    this.ctx = this.getElement('.species-circle canvas').getContext("2d")

	    this.tooltip = tooltip
	    this.moduleState = moduleState

	    this.coorsField = new CoordinatesField()
	    this.getElement('.coordinates-wrapper').appendChild(this.coorsField.e)
	    this.coorsField.setChangeListener(coors => {
	    	this.moduleState.updateAtomPosition(this.atomIndex, this.coorsField.getValues())
	    })

	    this.speciesField = this.getElement('.species-value')
	    this.speciesField.addEventListener('change', e => {
	    	let species = (this.speciesField.value === '' ? undefined : this.speciesField.value)
	    	this.moduleState.updateAtomSpecies(this.atomIndex, species)
	    })

	    this.speciesCircle = this.getElement('.species-circle canvas')
	    this.getElement('.species-circle').appendChild(this.tooltip.e)

	    this.speciesCircle.addEventListener('click', e => {
	    	this.speciesCircle.parentElement.appendChild(this.tooltip.e)
	    	this.tooltip.e.style.visibility = 'visible'
	    	this.tooltip.reset(this.initMomentValue, this.constraintChecked)
	    	this.tooltip.setAtomIndex(this.atomIndex)
	    })

	    this.getElement('.img-button').addEventListener('click', e => {
		  this.moduleState.removeAtom(this.atomIndex)
		  // The next atom falls into this index and needs to be highlighted (the mouse pointer is on its row)
		  this.moduleState.highlightAtom(this.atomIndex)  // If it was the last no one is highlighted
		})

	}


	/**
	 * Sets the atom values into the row fields
	 * @param {int} index Atom index in the structure
	 * @param {object} atom
	 */
	setValues(index, atom){
		this.atomIndex = index
		this.getElement('.atom-order').textContent = (index+1)+'.'
		this.coorsField.setValues( this.moduleState.getAtomPosition(index))
		this.speciesField.value = (atom.species === undefined ? '' : atom.species)
        this.initMomentValue = (atom.initMoment === undefined ? '' : atom.initMoment)
        this.constraintChecked = atom.constraint
		this.drawCircle(atom.species, (this.constraintChecked || (this.initMomentValue !== '' && this.initMomentValue !== 0)))
	}


	/**
	 * Draws the circle representing the species
	 * @param  {string} species
	 * @param  {boolean} stroke
	 */
	drawCircle(species, stroke = false){

		this.ctx.clearRect(0, 0, 18, 19)
	    this.ctx.beginPath()
	    if (species === undefined) this.ctx.fillStyle = 'black'
	    else this.ctx.fillStyle = Conf.getSpeciesColor(species)
	    this.ctx.arc(9, 10, 8, 0, 2*Math.PI, true)
	    this.ctx.fill()

	    if ( this.ctx.fillStyle.indexOf('ffff') > 0 ) { // IF clear color outline is drawn
	      this.ctx.lineWidth = 0.5;
	      this.ctx.strokeStyle = "#000";
	      this.ctx.stroke();
	    }

	    if (stroke){
	      this.ctx.lineWidth = 1.5;
	      this.ctx.strokeStyle = "#777";
	      this.ctx.stroke();

	      this.drawTriangle()
	    }
	}


	/**
	 * Draws a triangle
	 */
	drawTriangle(){

		this.ctx.beginPath();
		this.ctx.moveTo(0, 0);
		this.ctx.lineTo(0, 5);
		this.ctx.lineTo(5, 0);
		this.ctx.closePath();

		this.ctx.fillStyle = "#777";
		this.ctx.fill();
	}

}
