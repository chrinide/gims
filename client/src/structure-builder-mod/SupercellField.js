/**
 * @author Iker Hurtado, Sebastian Kokott
 *
 * @fileoverview File holding the SupercellField UI component
 */

import UIComponent from '../common/UIComponent.js'


/**
 * Editable field made up of 3 text inputs, one per dimension
 */
export default class SupercellField extends UIComponent{

  constructor(){

    super('span', '.SupercellField')

    this.setHTML(`
        <div>
        <input type="text" class="sc-value" />
        <input type="text" class="sc-value" />
        <input type="text" class="sc-value" />
        </div>
        <div class="supercell-row">
        <input type="text" class="sc-value matrix" style="display:none"/>
        <input type="text" class="sc-value matrix" style="display:none"/>
        <input type="text" class="sc-value matrix" style="display:none"/>
        </div>
        <div class="supercell-row">
        <input type="text" class="sc-value matrix" style="display:none"/>
        <input type="text" class="sc-value matrix" style="display:none"/>
        <input type="text" class="sc-value matrix" style="display:none"/>
        </div>
        <div class="supercell-matrix-checkbox">
             <input type="checkbox" class="isMatrix" onclick="">Use Supercell Matrix
        </div>
        <div class="create-button-div">
          <button class="create-button">Create Supercell</button>
        </div>
    `)

    let showFields = (e) => {
      let fields = this.getElements('input.matrix')
      if (e.target.checked) {
        fields.forEach(f => {f.style = ''})
      } else {
        fields.forEach(f => {f.style.display = 'none'})
      }
    }

    this.fields = this.getElements('input.sc-value')//e.querySelectorAll('input');
    // console.log('this.fields',this.fields)
    this.fields.forEach( f => f.addEventListener('input', inputEventHandler) )
    this.isMatrix = this.getElement('.isMatrix')
    this.isMatrix.addEventListener('change', showFields)
    // console.log(this.isMatrix);

    // this.fields.forEach( f => f.addEventListener('change', changeEventHandler) )

    function inputEventHandler(e){
      const value = parseInt(e.target.value)
      console.log('input event',e.target.value, value,isNaN(value))
      if (e.target.value === '-') {
        // Wait for next character
      } else if (isNaN(value)) {
        e.target.value = ''
      }
    }

  }


  /**
   * Returns the values, one per dimension
   * @return {Array<float>]}
   */
  getValues(){
    let a = []
    this.fields.forEach(f => {
      if (f.style.display !== 'none') {
        if (f.value === '') {
          a.push(0)
        } else {
          a.push(parseInt(f.value))
        }
      }
    })
    return a
    // [parseInt(this.fields[0].value),
      // parseInt(this.fields[1].value),
      // parseInt(this.fields[2].value) ]
  }


  /**
   * Sets the values, one per dimension
   * @param {Array<float>]} values
   */
  setValues(value){
    for (let i = 0; i < this.fields.length; i++)
      this.fields[i].value = value
  }


  // /**
  //  * Sets the listener that will be called when any dimension value is changed
  //  * The values will be passed in
  //  * @param {function} listener
  //  */
  // setChangeListener(listener){
  //   this.changeListener = listener
  // }

}
