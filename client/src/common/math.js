
/**
 * @author Iker Hurtado, Sebastian Kokott
 *
 * @fileoverview This is a basic math library.
 * It's coupled with the Three.js library in order to carry out
 * some geometry operations
 */

// import * as THREE from "../../lib/three.js";
import * as THREE from "three/build/three.min.js";

/**
 * Adds two vectors and returns a new one
 * @param {Array<float>} p1
 * @param {Array<float>} p2
 * @return {Array<float>}
 */
export function add(p1, p2){
    return [ p2[0]+p1[0], p2[1]+p1[1], p2[2]+p1[2] ]
}


/**
 * Adds up a vector (the second paramenter) to first one and returns it
 * @param {Array<float>} p1
 * @param {Array<float>} p2
 * @return {Array<float>} Returns p1
 */
export function addUp(p1, p2){
    p1[0] += p2[0]
    p1[1] += p2[1]
    p1[2] += p2[2]
    return p1
}


/**
 * Subtracts the second vector from the first one and returns a new one
 * @param {Array<float>} p1
 * @param {Array<float>} p2
 * @return {Array<float>}
 */
export function subtract(p2, p1){
	return [ p2[0]-p1[0], p2[1]-p1[1], p2[2]-p1[2] ]
}


/**
 * Multiplies a vector by a scalar number and returns a new vector
 * @param {Array<float>} vector
 * @param {float} scalar
 * @return {Array<float>}
 */
export function multiplyScalar(vector, scalar){
	return [ vector[0]*scalar, vector[1]*scalar, vector[2]*scalar ]
}


/**
 * Divides a vector by a scalar number and returns a new vector
 * @param {Array<float>} vector
 * @param {float} scalar
 * @return {Array<float>}
 */
export function divideScalar(vector, scalar){
    return [ vector[0]/scalar, vector[1]/scalar, vector[2]/scalar ]
}


/**
 * Returns the distance between two points
 * @param {Array<float>} p1
 * @param {Array<float>} p2
 * @return {float}
 */
export function getDistance(p1, p2){
	return Math.sqrt( (p1[0]-p2[0])**2 + (p1[1]-p2[1])**2 + (p1[2]-p2[2])**2)
}


/**
 * Returns the angle made up of three points
 * @param {Array<float>} p1
 * @param {Array<float>} p2
 * @return {float}
 */
export function getAngle(p1, p2, p3){
    let p12v = new THREE.Vector3().fromArray(subtract(p1, p2))
    let p32v = new THREE.Vector3().fromArray(subtract(p3, p2))
    p12v.angleTo(p32v)
    return (p12v.angleTo(p32v)*180)/Math.PI
}


/**
 * Returns the torsion angle comprised of four points
 * @param {Array<float>} p1
 * @param {Array<float>} p2
 * @param {Array<float>} p3
 * @return {float}
 */
export function getTorsionAngle(p1, p2, p3, p4){
	const a = new THREE.Vector3().fromArray(subtract(p2, p1))
    const b = new THREE.Vector3().fromArray(subtract(p3, p2))
    const c = new THREE.Vector3().fromArray(subtract(p4, p3))

    let bxa = new THREE.Vector3().crossVectors(b, a).normalize()
    let cxb = new THREE.Vector3().crossVectors(c, b).normalize()
    let angle = bxa.dot(cxb)
    // check for numerical trouble due to finite precision (copy/paste from python code)
    if (angle < -1) angle = -1
    else if (angle > 1) angle = 1

    angle = (Math.acos(angle)*180)/Math.PI
	if ( bxa.dot(c) > 0 ) angle = 360 - angle

    return angle
}


/**
 * Determines minimum or maximum (op=Math.min or =Math.max, respectively) of
 * array along certain axis.
 * @param  {Array<float>} mat Matrix
 * @param  {int} axis
 * @param  {[type]} op Operation minimum or maximum
 * @return {[type]}
 */
export function minmax(mat,axis,op){
    //
    let extr = []
    let [m,n] = getDim(mat)
    let l,matrix
    if (axis==0){
        matrix = mat[0].map((col, i) => mat.map(row => row[i])) //Transpose
        l = n
    }else {
        matrix = mat
        l = m}
    for (let i=0; i<l;i++){
        extr.push(op(...matrix[i]))
    }
    return extr
}


/**
 * Multiplies (dot) two matrices. Needs final array initialized (Much better performance).
 * @param {Array<float>} A
 * @param {Array<float>} B
 * @param {Array<float>} C Inicialized matrix
 * @return {Array<float>}
 */
export function matrixDot(A,B,C){
    for (let i in A) {
        for (let j in B) {
            let sum = 0
            for (let k in A[0]) {
                sum += A[i][k]*B[k][j]
            }
            C[i][j] = sum
        }
    }
    return C
}


/**
 * Multiplies an array with a constant. Returning array of dimension of arr.
 * Currently only flat arrays.
 * @param  {float} c
 * @param  {Array<float>} arr
 * @return {Array<float>}
 */
export function constantDot(c,arr){
    //
    let res = []
    for (let i=0;i<arr.length;i++){
        res.push(c*arr[i])
    }
    return res
}



/**
 * Not yet full tensor dot. Accepts vector B and matrix A.(Sebastian comment)
 * @param  {Array<float>} A
 * @param  {Array<float>} B
 * @return {Array<float>}
 */
export function tensorDot(A,B){
    //
    let C = []
    for (let i=0;i<A.length;i++){
        for (let j=0;j<B.length;j++){
            C.push(constantDot(A[i][0],B[j]))
        }
    }
    return C
}


/**
 * Returns the dimensions of a 2D matrix. If flat, returns 0 as first dimension.
 * @param {Array<float>} mat
 * @return {Array<float>}
 */
export function getDim(mat) {
    if (mat[0].length !== undefined){
        return [mat.length,mat[0].length]}
    else {
        return [0,mat.length] }
}


/**
 * Adds the values of two arrays and returns them in a new array
 * @param {Array<float>} arr1
 * @param {Array<float>} arr2
 * @return {Array<float>}
 */
export function addArrays(arr1,arr2){
    // Add two arrays.
    let arr = arr1.slice()
    for (let i in arr1) {
        arr[i] = arr1[i] +arr2[i]
    }
    return arr
}

export function determinant33(m){
  return m[0][0]*m[1][1]*m[2][2] +
         m[0][1]*m[1][2]*m[2][0] +
         m[0][2]*m[1][0]*m[2][1] -
         m[0][0]*m[1][2]*m[2][1] -
         m[0][1]*m[1][0]*m[2][2] -
         m[0][2]*m[1][1]*m[2][0]
}

export function invert33(m){
  var n11 = m[0][0], n21 = m[1][0], n31 = m[2][0],
      n12 = m[0][1], n22 = m[1][1], n32 = m[2][1],
      n13 = m[0][2], n23 = m[1][2], n33 = m[2][2],

      t11 = n33 * n22 - n32 * n23,
      t12 = n32 * n13 - n33 * n12,
      t13 = n23 * n12 - n22 * n13,

      det = n11 * t11 + n21 * t12 + n31 * t13;

  if (det === 0) {
    console.error( "invert3b3(m): det(m) == 0. m cannot be inverted!")
    return undefined
  }
  var detInv = 1 / det,
      i =[[0,0,0],[0,0,0],[0,0,0]];
  i[0][0] = t11 * detInv;
  i[1][0] = ( n31 * n23 - n33 * n21 ) * detInv;
  i[2][0] = ( n32 * n21 - n31 * n22 ) * detInv;

  i[0][1] = t12 * detInv;
  i[1][1] = ( n33 * n11 - n31 * n13 ) * detInv;
  i[2][1] = ( n31 * n12 - n32 * n11 ) * detInv;

  i[0][2] = t13 * detInv;
  i[1][2] = ( n21 * n13 - n23 * n11 ) * detInv;
  i[2][2] = ( n22 * n11 - n21 * n12 ) * detInv;

  return i
}

export function multiplyM33V3(m,v) {
  return [
    m[0][0]*v[0]+m[0][1]*v[1]+m[0][2]*v[2],
    m[1][0]*v[0]+m[1][1]*v[1]+m[1][2]*v[2],
    m[2][0]*v[0]+m[2][1]*v[1]+m[2][2]*v[2],
  ]
}

export function transpose33(m){
  return [
    [m[0][0],m[1][0],m[2][0]],
    [m[0][1],m[1][1],m[2][1]],
    [m[0][2],m[1][2],m[2][2]]
  ]
}

export function solve33(m,v) {
  var i = transpose33(invert33(m))
  return multiplyM33V3(i,v)

}
