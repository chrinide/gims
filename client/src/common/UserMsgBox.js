/**
 * @author Iker Hurtado
 *
 * @fileoverview File holding the UserMsgBox UI component
 */

import UIComponent from './UIComponent.js'


/**
 * Text box on the top of the application layout for warning or showing dynamic 
 * and relevant information to the user
 */
class UserMsgBox extends UIComponent{

  constructor(){
    super('div', '.UserMsgBox')
  }

}


/**
 * Creates the application instance (singleton pattern) and adds to the HTML body 
 * @type {UserMsgBox}
 */
const userMsgBox = new UserMsgBox()
document.querySelector('body').appendChild(userMsgBox.e)


/**
 * Sets a user message
 * @param {string} msg
 */
export function setMsg(msg){
  userMsgBox.e.innerHTML = msg
  userMsgBox.e.classList.remove('error')//userMsgBox.e.style.color = ''
  userMsgBox.e.style.display = 'block'

  setTimeout( () => userMsgBox.e.style.display = 'none', 3000)
} 


/**
 * Sets an application error for the user
 * @param {string} msg
 */
export function setError(msg){
  userMsgBox.e.innerHTML = msg
  userMsgBox.e.classList.add('error')//userMsgBox.e.style.color = 'red'
  userMsgBox.e.style.display = 'block'

  setTimeout( () => userMsgBox.e.style.display = 'none', 5000)
} 


