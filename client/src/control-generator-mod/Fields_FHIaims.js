export const Fields_FHIaims = [
  {
    label: 'Basic Settings',
    fields:{
      species: {
        // If there are no flags attributes the field name is taken ('species' in this case)
        text:'Geometry species',
        inputType:'input-text',
        required: true,
        dataType: 'string',
        explanation: `
            This field reflects the different species included in your structure.
            The ControlGenerator accepts any valid element of the periodic table.
        `,
        workflow: 'default'
      },
      xc: { // XC functional for FHIaims
        text:'XC functional',
        inputType:'select',
        required: true,
        value: [
          '',
          'pz-lda',
          'pw-lda',
          'pbe',
          'pbe+TS:{"xc":"pbe","vdw_correction_hirshfeld":true}',
          'pbe+MBD:{"xc":"pbe","many_body_dispersion":""}',
          'pbesol',
          'pbe0',
          'pbe0+TS:{"xc":"pbe","vdw_correction_hirshfeld":true}',
          'pbe0+MBD:{"xc":"pbe0","many_body_dispersion":""}',
          'hse06:{"xc":"hse06 0.11","hse_unit":"bohr"}',
          'hse06+TS:{"xc":"hse06 0.11","hse_unit":"bohr","vdw_correction_hirshfeld":true}',
          'hse06+MBD:{"xc":"hse06 0.11","hse_unit":"bohr","many_body_dispersion":""}',
          'hf'
        ],
        dataType: 'string',
        explanation: `
            Select the exchange correlation (XC) functional for your calculation. <br>
            <b>pz-lda:</b> Perdew-Zunger LDA<br>
            <b>pw-lda:</b> Perdew-Wang LDA<br>
            <b>pbe:</b> Perdew-Burke-Ernzerhof GGA<br>
            <b>pbe+TS:</b> pbe + Tkatchenko-Scheffler vdW correction<br>
            <b>pbe+MBD:</b> pbe + Many-Body-Disperion correction<br>
            <b>pbesol:</b> pbe optimized for solids<br>
            <b>pbe0:</b> pbe + hf hybrid functional<br>
            <b>pbe0+TS:</b> pbe + hf hybrid functional + Tkatchenko-Scheffler vdW correction<br>
            <b>pbe0+MBD:</b> pbe + hf hybrid functional + Many-Body-Disperion correction<br>
            <b>hse06:</b> Heyd-Scuseria-Ernzerhof screened exchange hybrid functional<br>
            <b>hse06+TS:</b> hse06 + Tkatchenko-Scheffler vdW correction<br>
            <b>hse06+MBD:</b> hse06 + Many-Body-Disperion correction<br>
            <b>hf:</b> Hartee-Fock method
        `,
        workflow: 'default'
      },
      kgrid: {
        flag: 'k_grid',
        text:'k-points grid',
        group: 0,
        inputType:'input-text:3',
        required: false, // REquired in some cases not always
        dataType: ['integer','integer','integer'],
        explanation: 'Enter the number of k-points along each reciprocal lattice vector.',
        workflow: 'default'
      },
      basisSettings: { // FHIaims only
        text:'Species Default Settings',
        group: 0,
        inputType:'select',
        required: true,
        value: ['', 'light', 'intermediate', 'tight', 'really_tight'],
        dataType: 'string',
        explanation: `
            The species defaults are a collection of pre-defined numerical parameters for the integration grids and basis functions.<br>
            <b>light:</b> Settings for fast prerelaxations, structure searches, etc. Usually, no obvious geometry or convergence errors resulted from these settings. Recommended for many household tasks.<br>
            <b>intermediate:</b> Only available for a few elements, but can play an important role for large and expensive calculations, especially for hybrid functionals.<br>
            <b>tight:</b> Regarding the integration grids, Hartree potential, and basis cutoff potentials, the settings specified here are rather safe, intended to provide meV-level accurate energy differences also for large structures.
        `,
        workflow: 'default'
      }
    },
  }, // End BasicSettings

  {
    label: 'Output',
    fields: {
      bandStructure: {
        text:'Band Structure',
        inputType:'input-text:1:k-point density',
        dataType: 'float',
        required: true,
        explicitInclusion: false,
        explanation: `
            Specifies the k-point density (Number of k-points times AA) for the band path. Recommendation: 40.
        `,
        units: [`k-points &sdot; &#197;`],
        workflow: 'BandStructure'
      },
      dos: {
        flag: 'output dos',
        text:'DOS',
        inputType:'input-text:4:Energy start,Energy end,Number of sampling points,Broadening',
        required: false,
        explicitInclusion: 'checkbox',
        dataType: ['float,','float','integer','float'],
        explanation: `
            Writes the density of states (DOS) to an external file for plotting purposes.<br>
            <b>Energy start:</b> Lower bound of single-particle energy range for which DOS is given. <br>
        `,
        units: ['eV', 'eV', ' ', 'eV'],
        workflow: 'default'
      }
    }
  }, // End output

  {
    label: 'Structure Optimization, Forces, and Stress',
    fields: {
      relaxGeometry: { // Only for FHIaims
        flag: 'relax_geometry trm',//'relax_geometry bfgs',
        text:'Relax Atom Positions',
        inputType:'input-text',
        value: '5e-3',
        required: false,
        explicitInclusion: 'checkbox',
        dataType: 'float',
        explanation: `
            Switches on the optimization of the atom positions. The field specifies the maximum residual force component per atom (in eV/AA). FHI-aims uses a trust radius method enhanced version of the BFGS optimization.
        `,
        units: `eV/&#197;`,
        workflow: 'default'
      },
      relaxUnitCell: { // Only for FHIaims
        flag: 'relax_unit_cell',
        text:'Relax Unit Cell',
        inputType:'select',
        value: ['full','fixed_angles'],
        dataType: 'string',
        required: false,
        explicitInclusion: 'checkbox',
        explanation: `Switches on the optimization of the lattice vectors. Works only, if <b>Relax Atom Positions</b> is switched on, too.`,
        workflow: 'default'
      },
      calculateForces: {
        flag: 'sc_accuracy_forces',
        text:'Calculate Forces',
        inputType:'input-text',
        value: '1e-3',
        dataType: 'float',
        required: false,
        explicitInclusion: 'checkbox',
        explanation: `
           Activates the calculation of the forces at the end of an SCF cycle, only. The field speciefies the convergence criteria for the forces (in eV/AA).
        `,
        units: `eV/&#197;`,
        workflow: 'default'
      },
      calculateStresses: { // Only for FHIaims
        flag: 'compute_analytical_stress',
        text:'Calculate Stress',
        inputType:'no-input',
        required: false,
        dataType: 'boolean',
        explicitInclusion: 'checkbox',
        explanation: `
            Switches on the computation of the analytical stress tensor.
        `,
        workflow: 'default'
      },
    }
  }, // End OptimizationForcesStress

  {
    label: 'Numerical Accuracy (optional)',
    info:'Note: FHI-aims figures out the SCF convergence parameters on its own. Please only set the below input parameters, if you have a good reason.',
    fields: {
      totalEnergyAccuracy: {
        flag: 'sc_accuracy_etot',
        text:'Total Energy Accuracy',
        inputType:'input-text',
        dataType:'float',
        required: false,
        explicitInclusion: 'checkbox',
        explanation: `
           Sets the SCF-convergence criteria for the total energy (in eV).
        `,
        units: `eV`,
        workflow: 'default'
      },
      totalDensityAccuracy: {
        flag: 'sc_accuracy_rho',
        text:'Total Density Accuracy',
        inputType:'input-text',
        dataType:'float',
        required: false,
        explicitInclusion: 'checkbox',
        explanation: `
           Sets the SCF-convergence criteria for the total electron density (in e/AA^3).
        `,
        units: `e/&#197;<sup>3</sup>`,
        workflow: 'default'
      },
      eigenvalueAccuracy: {
        flag: 'sc_accuracy_eev',
        text:'Eigenvalue Accuracy',
        inputType:'input-text',
        dataType:'float',
        required: false,
        explicitInclusion: 'checkbox',
        explanation: `
           Sets the SCF-convergence criteria for the sum of the eigenvalues (in eV).
        `,
        units: `eV`,
        workflow: 'default'
      },
      forcesAccuracy: {
        flag: 'sc_accuracy_forces',
        text:'Forces Accuracy',
        inputType:'input-text',
        dataType:'float',
        required: false,
        explicitInclusion: 'checkbox',
        explanation: `
           Sets the SCF-convergence criteria for the forces (in eV/AA).
        `,
        units: `eV/&#197;`,
        workflow: 'default'
      }
    }
  } // End numericalAccuracy
]
