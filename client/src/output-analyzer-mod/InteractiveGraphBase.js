/**
 * @author Iker Hurtado
 *
 * @fileoverview File holding the InteractiveGraphBase graph base
 */

import Canvas from '../common/Canvas.js'


const FONT = 'Roboto'
const FONT_SIZE_NORMAL = 18
const BACKGROUND_DEFAULT = '#FFFFFF'
const LABELS_COLOR_DEFAULT = '#000000'
const TICK_LABEL_SIZE_DEFAULT = 1
const AXIS_LABEL_SEPARATION = 50
const X_AXIS_TICK_LABEL_TOP_SEPARATION = 8
const Y_AXIS_TICK_LABEL_LEFT_SEPARATION = 7


/**
 * This is the base class that models a highly interactive plotter.
 * It is inherited by several classes implementing specifc plotters.
 * This plotter implements zoom and y-axis offset
 */
export default class InteractiveGraphBase{

  /**
   * @param  {object} margin Object like {left: , right: , top: , bottom: }
   */
  constructor(margins = {left: 20, right: 0, top: 0, bottom: 20}) {
    this.margins= margins;
    this.parentElement= null;
    this.plotContent = null;
    this.axisGroup = null;
    this.yAxisLabelsGroup = null;
    this.noDataGroup = null;

    this.yZoom = 1;  // Initial zoom
    this.yOffset = 0; // Initial y offset = 0
    this.repaintListener
    this.nodataLabel = null;
    this.lines = new Map()//[]
    this.lineStyles = new Map()//this.linesProps = []
  }


  /**
   * Creates and attachs the graph canvas inside a HTML layout element. 
   * Sets the width and height of the canvas, and optionaly the 
   * canvas resolution can be doubled
   * @param  {HTMLElement} element
   * @param  {int} width
   * @param  {int} height
   * @param  {boolean} doubleRes
   */
  attach(element, width, height, doubleRes = false){
    this.c = new Canvas(undefined, '')
    this.canvas = this.c.canvas
    this.c.setCursorOnElement('pointer')
    this.parentElement= element;
    this.parentElement.appendChild(this.canvas);
    this.width = (width !== undefined ? width : this.parentElement.clientWidth);
    this.height = (height !== undefined ? height : this.canvas.width);
    this.canvas.style.width = this.width+'px'
    this.canvas.style.height = this.height+'px'
    if (doubleRes) { this.width *= 2; this.height *= 2}
    this.canvas.width = this.width
    this.canvas.height = this.height

    this.plotAreaWidth = this.width - this.margins.left - this.margins.right;
    this.plotAreaHeight = this.height - this.margins.bottom - this.margins.top;
    this.plotRangeX = this.plotAreaWidth
    this.plotRangeY = this.plotAreaHeight

    // Event handling
    this.c.canvas.addEventListener('wheel', e => {
      e.preventDefault()
      if (e.deltaY > 0 && this.yZoom > 0.05 ) this.yZoom *= 0.83 //-= 0.09;
      else if (e.deltaY < 0 && this.yZoom < 10) this.yZoom *= 1.2//+= 0.2;
      this.draw()
      if (this.repaintListener)
        this.repaintListener(this.yZoom, this.yOffset)
    });

    let initPosY;
    this.c.canvas.addEventListener('mousedown', e => {
      e.preventDefault();
      initPosY = e.clientY + this.yOffset;
      //console.log('mousedown: e.clientY + this.yOffset', e.clientY, this.yOffset);
      this.c.canvas.addEventListener('mousemove', moveListener);

      this.c.canvas.addEventListener('mouseup', e => {
        this.c.canvas.removeEventListener('mousemove', moveListener);
      });
      this.c.canvas.addEventListener('mouseout', e => {
        this.c.canvas.removeEventListener('mousemove', moveListener);
      });
    });

    let self = this;
    function moveListener(e) {
      //console.log('Y offset:', e.clientY - initPosY);
      // Bad if (initPosY - e.clientY > this.yMax || initPosY - e.clientY < this.yMin)
      self.yOffset = initPosY - e.clientY ;
      self.precalculation_2 = self.precalculation_1 - self.yOffset;
      self.draw();
      if (self.repaintListener)
        self.repaintListener(self.yZoom, self.yOffset);
    }

  }

  /**
   * Check if the graph is already attached
   * @return {boolean} 
   */
  isAttached(){
    return this.parentElement !== null;
  }

  
  /**
   * Sets the labels and value range for the axes
   * @param {string} xLabel X axis lable
   * @param {boolean} xTicks Indicates if the x axis has ticks
   * @param {float} xMin X axis minimun value 
   * @param {float} xMax X axis maximun value
   * @param {string} yLabel Y axis lable
   * @param {float} yMinInit Y axis initial minimun value 
   * @param {float} yMaxInit Y axis initial maximun value 
   * @param {float} yMin Y axis absolute minimun value 
   *   (minimun value reachable using the interaction)
   * @param {float} yMax Y axis absolute maximun value 
   *   (maximun value reachable using the interaction)
   * @param {float} yLabelGap Initial Y axis gap between ticks
   * @param {int} 
   */
  setAxisRangeAndLabels(xLabel, xTicks, xMin, xMax, yLabel, 
    yMinInit, yMaxInit,yMin, yMax, yLabelGap){

    this.xLabel= xLabel;
    this.yLabel = yLabel
    this.xMin = xMin;
    this.xMax // Initialized below
    this.yMinInit= yMinInit;
    this.yMaxInit= yMaxInit;
    this.yMin= yMin;
    this.yMax= yMax;
    this.yLabelGapInit = yLabelGap;

    let xSteps, exp
    if (xTicks){
      let t = generateDiagramSteps(xMax)
      xSteps = t[0]; exp = t[1]
      this.xMax = xSteps[xSteps.length-1]
    }else
      this.xMax = xMax

    this.xRel= this.plotRangeX/(this.xMax-this.xMin);
    this.yRel= this.plotRangeY/(this.yMaxInit-this.yMinInit);

    // Reset canvas
    this.c.reset();
    // Dynamic elements: they change with interaction cahnges
    this.c.addGroup('yAxisLabel')
    this.c.addGroup('data')
    this.c.addGroup('yAxisTicks')
    this.c.addGroup('xAxisTicks')
    // Static elements: they don't change with interaction but they ha to be redrawn
    this.c.addGroup('static') // graph box and axes labels and x axis ticks

    // Reset local (this instance) data
    this.yZoom = 1;  // Initial zoom
    this.yOffset = 0; // Initial y offset = 0
    this.lines = new Map()//[]
    this.lineStyles = new Map()//this.linesProps = []

    // Draw axes

    // X axis drawing and parallel line
    const xAxisY = this.plotAreaHeight+this.margins.top
    this.xAxisY = xAxisY
    const left = this.margins.left
    const style = {strokeColor: 'black', strokeWidth: 0.5}
    let coors = [left, xAxisY, left+this.plotAreaWidth, xAxisY]
    this.c.addElement('line', {coors: coors, style: style }, 'static')
    coors = [left, this.margins.top, left+this.plotAreaWidth, this.margins.top]
    this.c.addElement('line', {coors: coors, style: style }, 'static')
    // Y axis drawing and parallel line
    const leftPlusWidth = this.margins.left+this.plotAreaWidth
    coors = [this.margins.left, xAxisY, this.margins.left, this.margins.top]
    this.c.addElement('line', {coors: coors, style: style }, 'static')
    coors = [leftPlusWidth, xAxisY, leftPlusWidth, this.margins.top]
    this.c.addElement('line', {coors: coors, style: style }, 'static')

    // Paint x and y axes labels

    // Y label dynamic
    this.yLabelSizeMult = 1
    this.yLabelColor = LABELS_COLOR_DEFAULT
    this.yLabelText = this.yLabel
    // Static
    if (xLabel)
      this.xLabelText = this.c.addElement('text', { 
        text: this.xLabel, 
        x: this.margins.left+this.plotAreaWidth/2, 
        y: xAxisY+AXIS_LABEL_SEPARATION, 
        style: {font: FONT_SIZE_NORMAL+'px '+FONT, align: 'center', baseline: 'top'} 
      } , 'static')

    // X axis ticks
    this.xTicksLabels = []
    if (xTicks)
      for (let i = 0; i < xSteps.length; i++) {
        let stepX = this.margins.left+((this.plotRangeX*xSteps[i])/this.xMax) 
        this.c.addElement('line', {
          coors: [stepX, xAxisY, stepX, xAxisY+3], 
          style: {strokeColor: 'black'} 
        }, 'xAxisTicks')
        this.xTicksLabels.push(this.c.addElement('text', { 
          text: (i === 0 ? '0' : xSteps[i].toFixed(exp)), 
          x: stepX, 
          y: xAxisY+X_AXIS_TICK_LABEL_TOP_SEPARATION, 
          style: {font: FONT_SIZE_NORMAL+'px '+FONT, align: 'center', baseline: 'top'} 
        } , 'xAxisTicks'))
      }

    // Y axis tick size
    this.yTickLabelsSize = TICK_LABEL_SIZE_DEFAULT

    // plotY precalcultation 1,
    this.precalculation_1 = this.plotAreaHeight  + this.yMinInit*this.yRel
    this.precalculation_2 = this.precalculation_1

    function generateDiagramSteps(maxVal){

      let d = 4; // generates 0 and 4 more points
      let exp = -Math.floor(Math.log(maxVal/d) * Math.LOG10E);

      let factor = Math.pow(10,exp);//100;
      let ceil = Math.ceil(maxVal*factor/d);
      let stepArray = [];
      for (var i = 0; i <= d; i++) {
        stepArray[i] = ceil*i/factor;
      }
      exp = (exp < 0 ? 0 : exp);
      return [stepArray, exp];
    }
  }

  /**
   * Sets a tick text on the X axis
   * @param {int} x 
   * @param {tickText}
   */
  setXAxisTickText(x, tickText){
    this.xTicksLabels.push(this.c.addElement('text', { 
      text: tickText, 
      x: this.plotX(x), 
      y: this.xAxisY+X_AXIS_TICK_LABEL_TOP_SEPARATION, 
      style: {
        font: (this.yTickLabelsSize*FONT_SIZE_NORMAL)+'px '+FONT, 
        align: 'center', 
        baseline: 'top'
      } 
    }, 'xAxisTicks' ))
  }


  /**
   * Adds a group of lines
   * @param {string} name Group name
   * @param {string} style Line style
   */
  addLineGroup(name, style){
    this.lines.set(name, [])
    if (style.defaultColor) style.strokeColor = style.defaultColor
    this.lineStyles.set(name, style)
  }

  /**
   * Adds a line (data) to a group of lines
   * @param {Array<float>}
   * @param {string}
   */
  setLineData(lineData, group){
    this.lines.get(group).push(lineData)
    //this.linesProps.push(props)
  }


  /**
   * Removes and draws the dynamic elements on the canvas
   */
  draw(){
    // remove the dynamic elements (they change with interaction) on the canvas
    this.c.removeGroupElements('yAxisLabel')
    this.c.removeGroupElements('yAxisTicks')
    // Y axis ticks
    this.c.removeGroupElements('data')

    // recalculate and add the dynamic elements on the canvas

    // add the Y axis ticks to the canvas
    let yLabelGap;
    // if (this.yZoom > 5) yLabelGap = this.yLabelGapInit/25;
    if (this.yZoom > 1) yLabelGap = this.yLabelGapInit/5;
    else if (this.yZoom < 0.2) yLabelGap = this.yLabelGapInit*10;
    else yLabelGap = this.yLabelGapInit;

    let min = Math.floor(this.yMin/yLabelGap)*yLabelGap;
    let max = Math.ceil(this.yMax/yLabelGap)*yLabelGap;
    this.yAxisMax = max;

    // Performance optimization
    const visibleMaxY = this.getYFromCanvasY(this.margins.top)
    const visibleMinY = this.getYFromCanvasY(
      this.plotAreaHeight+this.margins.top)

    this.yTickTexts = []
    //log('visibleMinY,visibleMaxY',visibleMinY,visibleMaxY)
    for (let i = min; i < max+1; i = i + yLabelGap) {
      let y = this.plotY(i)  // If it's out of the plot area it's not included into the canvas
      if (visibleMinY < i && i < visibleMaxY){ // Significant performance optimization
        this.c.addElement('line', {
          coors: [this.margins.left-3, y, this.margins.left, y], 
          style: {strokeColor: 'black'} 
        }, 'yAxisTicks')
        this.yTickTexts.push(this.c.addElement('text', { 
          text: i, 
          x: this.margins.left-Y_AXIS_TICK_LABEL_LEFT_SEPARATION, 
          y: y, 
          style: {
            font: (this.yTickLabelsSize*FONT_SIZE_NORMAL)+'px '+FONT, 
            align: 'end', 
            baseline: 'middle'
          } 
        }, 'yAxisTicks' ))
      }
    }

    // Add the Zero line
    this.c.addElement('line', {
      coors: [this.plotX(0), this.plotY(0), this.plotX(0)+this.plotAreaWidth,  this.plotY(0)], 
      style: {strokeColor: 'gray'} 
    }, 'data')

    // add the data graph info
    this.lines.forEach( (lines, groupName) => {
      
      let style = this.lineStyles.get(groupName)
      //log('lines, groupName', groupName, style, lines)
      let polyline = []
      lines.forEach( lineData => {    
        let points = []
        for (let i = 0; i < lineData.length; ) {
          points.push(this.plotX(lineData[i++]))
          points.push(this.plotY(lineData[i++]))
        }
        polyline.push(points)
      })
      // A polyline is drawn for the sake of performance
      this.c.addElement('polyline', {lines: polyline, style: style}, 'data')
    })

    this.yLabelElement = this.c.addElement('text',{ 
      text: this.yLabelText, 
      x: this.margins.left-AXIS_LABEL_SEPARATION, 
      y: this.margins.top+this.plotAreaHeight/2, style: { 
        font: (this.yLabelSizeMult*FONT_SIZE_NORMAL)+'px '+FONT, 
        color: this.yLabelColor, 
        align: 'center', 
        baseline: 'bottom', 
        rotation: -Math.PI/2 
      } 
    } , 'yAxisLabel')

    // the effective draw on the canvas 
    this._draw()
  }


  /**
   * Effective draw on the canvas (the groups drawing order means)
   */
  _draw(){
    this.c.clearCanvas()
    this.c.fillCanvas(BACKGROUND_DEFAULT)
    // First the data lines are drawn
    this.c.drawGroup('data')
    // The top and bottom stripes are removed (data lines out of the graphical area)
    this.c.fillRect(BACKGROUND_DEFAULT, 0, 0, this.c.canvas.width, this.margins.top)
    this.c.fillRect(BACKGROUND_DEFAULT, 0, this.c.canvas.height-this.margins.bottom, 
      this.c.canvas.width, this.margins.bottom)
    // The rest of elements are drawn
    this.c.drawGroup('static')
    this.c.drawGroup('xAxisTicks')
    this.c.drawGroup('yAxisTicks')

    let maxTextWidth = 0
    this.yTickTexts.forEach( text => {
      if (text.boxWidth && (parseFloat(text.boxWidth.width) > maxTextWidth) )
          maxTextWidth = parseFloat(text.boxWidth.width)
    })
    this.yLabelElement.x = this.margins.left-AXIS_LABEL_SEPARATION-maxTextWidth
    
    this.c.drawGroup('yAxisLabel')
  }


  /**
   * Returns the value of the y coordinate from the canvas Y coordinate
   * @param  {float}
   * @return {float}
   */
  getYFromCanvasY(canvasY){
    return -(canvasY-this.precalculation_2)/(this.yZoom*this.yRel)
  }

  
  /**
   * Returns the canvas y value from the regular y value 
   * @param  {float}
   * @return {float}
   */
  plotY(y){
    // Precalculation usage
    // this.plotAreaHeight -y*this.yZoom*this.yRel + this.yMinInit*this.yRel - this.yOffset
    // -y*this.yZoom*this.yRel (calculated here)  this.plotAreaHeight  + this.yMinInit*this.yRel - this.yOffset
    let result = -y*this.yZoom*this.yRel + this.precalculation_2;
    return result;
  }


  /**
   * Returns the canvas x value from the regular x value 
   * @param  {float}
   * @return {float}
   */
  plotX(x){
    return this.margins.left + x*this.xRel
  }


  /**
   * Sets a listener function that will be called for every canvas repaint
   * The y axis zoom and offset will be passed in
   * @param {function} listener 
   */
  setRepaintListener(listener) {
    this.repaintListener = listener;
  }


  /**
   * Sets the default styles and Y axis zoom
   * @param {float} labelsFontMult Labels size font multiplier
   * @param {float} ticksFontMult Tick numbers size font multiplier
   * @param {object} linesThicknessObj Object featuring the lines thickness
   */
  setDefaultStyleAndZoom(labelsFontMult = 1, 
    ticksFontMult = TICK_LABEL_SIZE_DEFAULT, linesThicknessObj){

    this.yLabelSizeMult = labelsFontMult // Dynamic 

    if (this.xLabelText) this.xLabelText.style.font = 
      (labelsFontMult*FONT_SIZE_NORMAL)+'px '+FONT

    this.yLabelColor = LABELS_COLOR_DEFAULT// Dynamic 

    if (this.xLabelText) this.xLabelText.style.color = LABELS_COLOR_DEFAULT
    this.yLabelText = this.yLabel // Dynamic 

    if (this.xLabelText) this.xLabelText.text = this.xLabel

    this.changeTickLabelsStyle(ticksFontMult)

    this.lines.forEach( (lines, groupName) => {
      //log('lines, groupName',lines, groupName, this.lineStyles)
      if (linesThicknessObj.groups.includes(groupName)){
        let style = this.lineStyles.get(groupName)
        style.strokeColor = style.defaultColor
        style.strokeWidth = linesThicknessObj.thicknessMult
      }
    })

    this.setYZoomAndOffset(1, 0)
    this.draw() 
  }


  /**
   * Sets the Y axis zoom and offset
   * @param {float} yZoom
   * @param {float} yOffset
   * @param {boolean} draw If true the canvas is drawn
   */
  setYZoomAndOffset(yZoom, yOffset, draw = false){
    //log('setYZoomAndOffset', yZoom, yOffset)
    this.yZoom = yZoom;
    this.yOffset = yOffset;
    this.precalculation_2 = this.precalculation_1 - this.yOffset;
    if (draw) this.draw()
  }


  /**
   * Changes the axes labels and the title styles
   * @param  {float} sizeMult Font size multiplier
   * @param  {string} color Color code
   */
  changeLabelsAndTitleStyle(sizeMult, color){
    //log('changeLabelsAndTitleStyle', sizeMult, color)
    if (sizeMult){
      this.yLabelSizeMult = sizeMult// Dynamic  
      if (this.xLabelText) // Static
        this.xLabelText.style.font = (sizeMult*FONT_SIZE_NORMAL)+'px '+FONT
    }
    if (color){
      this.yLabelColor = color// Dynamic 
      if (this.xLabelText) this.xLabelText.style.color = color
    }
    this.draw()
  }


  /**
   * Changes the axes labels texts
   * @param  {string} xAxisLabel
   * @param  {string} yAxisLabel
   */
  changeLabelsText(xAxisLabel, yAxisLabel){
    if (xAxisLabel) this.xLabelText.text = xAxisLabel
    if (yAxisLabel) this.yLabelText = yAxisLabel// Dynamic
    this.draw()
  }


  /**
   * Changes the tick labels style
   * @param  {float} sizeMult Font size multiplier
   * @param  {string} color Color code
   * @param {boolean} draw If true the canvas is drawn
   */
  changeTickLabelsStyle(sizeMult, color, draw = false){
    this.xTicksLabels.forEach( label => {
      if (sizeMult) label.style.font = (sizeMult*FONT_SIZE_NORMAL)+'px '+FONT
      if (color) label.style.color = color
    })
    this.yTickLabelsSize = sizeMult
    if (draw)  this.draw()
  }


  /**
   * Changes the data lines style
   * @param  {float} thicknessMult Lines thickness multiplier
   * @param  {string} color Color code
   * @param  {string} group Lines group
   */
  changeDataLinesStyle(thicknessMult, color, group){
    if (thicknessMult){
      let style = this.lineStyles.get(group)
      if (style) style.strokeWidth = thicknessMult
    }
    if (color){
      let style = this.lineStyles.get(group)
      if (style) style.strokeColor = color
    }
    this.draw()
  }

}
