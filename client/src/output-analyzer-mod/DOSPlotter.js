
/**
 * @author Iker Hurtado
 *
 * @fileoverview File holding the DOSPlotter graph
 */

import InteractiveGraphBase from './InteractiveGraphBase.js'


/**
 * A plotter displaying the DOS data
 */
export default class DOSPlotter{

  /**
   * @param  {object} margin Object like {left: , right: , top: , bottom: }
   * @param  {string} xAxisLabel X axis label
   * @param  {string} yAxisLabel Y axis label
   * @param  {string} spin1Color Spin 1 plot color
   * @param  {string} spin1Color Spin 2 plot color
   */
  constructor(margins, xAxisLabel, yAxisLabel, spin1Color = '#000000', spin2Color = '#000000') {
    this.baseGraph = new InteractiveGraphBase(margins)
    this.xAxisLabel = xAxisLabel
    this.yAxisLabel = yAxisLabel
    this.spin1Color = spin1Color
    this.spin2Color = spin2Color
  }


  /**
   * Creates and attachs the graph canvas inside a HTML layout element.
   * Sets the width and height of the canvas, and optionaly the
   * canvas resolution can be doubled
   * @param  {HTMLElement} element
   * @param  {int} width
   * @param  {int} height
   * @param  {boolean} doubleRes
   */
  attach(element, width, height, doubleRes = false){
    //super.attach(element, width, height);
    this.baseGraph.attach(element, width, height, doubleRes);
  }


  /**
   * Sets the DOS data to be plotted.
   * Also the top and bottom X values can be passed
   * @param {object} data
   * @param {int} eAxisMin
   * @param {int} eAxisMax
   */
  setData(data, eAxisMin = -6, eAxisMax = 12){

    this.pointsSpin1 = []
    this.pointsSpin2 = []

    let havingSpin = (data[0].length === 3)
    let pointsXInPlotRange = []
    let pointsYInPlotRange = []

    for (let i = 0; i < data.length; i++) {
      let energy = data[i][0]// /E_FACTOR;
      pointsYInPlotRange.push(energy);

      pointsXInPlotRange.push(data[i][1])
      this.pointsSpin1.push(data[i][1])
      this.pointsSpin1.push(energy)

      if (havingSpin){
        pointsXInPlotRange.push(data[i][2])
        this.pointsSpin2.push(data[i][2])
        this.pointsSpin2.push(energy)
      }
    }

    let maxDosVal = Math.max.apply(null, pointsXInPlotRange);
    let maxEnergyVal = Math.max.apply(null, pointsYInPlotRange);
    let minEnergyVal = Math.min.apply(null, pointsYInPlotRange);

    this.baseGraph.setAxisRangeAndLabels(this.xAxisLabel, true, 0, maxDosVal, this.yAxisLabel,
      eAxisMin, eAxisMax, minEnergyVal, maxEnergyVal, 5);

    this.baseGraph.addLineGroup('spin1', {defaultColor: this.spin1Color})
    this.baseGraph.setLineData(this.pointsSpin1, 'spin1')//, lineWidth: 2})//[{x:1,y:1}, {x:5, y:5}])//
    if (havingSpin){
      this.baseGraph.addLineGroup('spin2', {defaultColor: this.spin2Color})
      this.baseGraph.setLineData(this.pointsSpin2, 'spin2')
    }

    this.baseGraph.draw()
  }


  /**
   * Sets a listening function before every repainting
   * @param {function} listener
   */
  setRepaintListener(listener) {
    this.baseGraph.repaintListener = listener
  }

}
