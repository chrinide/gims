/**
 * @author Iker Hurtado, Sebastian Kokott
 *
 * @fileoverview File holding the OutputAnalyzerMod class and module
 */


import UIComponent from '../common/UIComponent.js'
import FileImporter from '../common/FileImporter.js'
import * as Chart from "chart.js/dist/Chart.min.js"
import * as ModalPopup from '../common/ModalPopup.js'//import {showModal, setModalContent} from '../common/ModalPopup.js'
import StructureViewer from "../structure-viewer/StructureViewer.js"
import {getOutputInstance, getHtmlRows} from './util.js'
import PaginationSelector from '../common/PaginationSelector.js'
import * as UserMsgBox from '../common/UserMsgBox.js'
import DOSPlotter from './DOSPlotter.js'
import BSPlotter from './BSPlotter.js'
import Form from '../common/Form.js'
import ScreenshotTaker from '../common/ScreenshotTaker.js'
import BZViewer from "../bz-viewer/BZViewer.js"

import downloadIcon from '../../img/download-icon.png'
import refreshIcon from '../../img/refresh-icon.png'


let init_html = `

	<h1> Output Analyzer </h1>

	<div class="outputanalyzer-importer-ph" > <div> </div>  <label><input type="checkbox"/>Select a folder containing the files</label> </div>

	<div class="no-output-box" style="display: none; ">
	</div>

	<div class="output-file-content" style="display: none">

		<div class="system-info-section page-section" style="margin-bottom: 0" >

			<div style="width: 40%">

				<table class="system-info-fields set-of-fields" style="width: 360px">
						<tr><td>Chemical Formula</td> <td><span class="formula-value"></span></td></tr>
						<tr><td>Number of atoms</td> <td><span class="atom-num-value"></span></td></tr>
				</table>

				<div class="page-section" style="margin-bottom: 0">

					<div class="page-section-title">Results</div>

					<table class="results-info-fields set-of-fields" style="width: 360px">
					</table>

				</div>

			</div>

			<div class="geometry-viz-ph" > </div>

		</div>

	</div>

	<div class="dos-bs-box page-section" style="display: none">
		<div class="page-section-title">DOS and Band Structure graphs</div>
		<div class="bs-error-msg" > </div>
		<div class="section-content" style="justify-content: space-evenly;"></div>
	</div>

	<div class="bz-viewer-box page-section" style="display: none;">
		<div class="bz-viewer-ph" style="justify-content: space-evenly;"></div>
	</div>

	<div class="output-file-content" style="display: none">

		<div class="page-section" >

			<div class="page-section-title">Summary Calculation</div>

			<div class="section-content">

				<table class="calculation-info-fields set-of-fields" style="width: 350px"></table>

				<div style="width: 60%">
					<div class="relaxation-graph-ph" style="padding-bottom: 20px"></div>  <!-- JavaScript processing-->
					<div class="convergence-graph-ph" ></div> <!-- JavaScript processing-->
				</div>

			</div>

		</div>

	</div>

	<div class="input-files-section" style="display: none">

		<div class="page-section" >

			<div class="page-section-title">Input files</div>

			<div class="section-content input-files-box" style="justify-content: space-evenly;">
			</div>

		</div>

	</div>

	<div class="parsing-files-section" style="display: none">
		<div class="page-section" >
			<div class="page-section-title">Parsing Errors</div>
			<div class="section-content">
				<table class="parsing-info-fields set-of-fields" style="width: 350px"></table>
			</div>
		</div>
	</div>
`

const NO_MAIN_FILE_TEXT = 'No main output file has been detected'
const NO_FILE_TEXT = 'No parsable file detected'
const PARSING_ERROR = `
	Something unexpectedly went wrong when parsing the main output file.<br>
	Please raise an issue in the <a href="https://gitlab.com/gims-developers/gims/-/issues" target="_blank">GIMS-gitlab</a>. Sorry for any inconvenience!
`

/**
 * OutputAnalyzer application module UI component
 */
export default class OutputAnalyzerMod extends UIComponent{

	constructor() {
    super('div', '#OutputAnalyzerMod')
    this.setHTML(init_html)

    this.importer = new FileImporter('Import output files:', true)
    this.getElement('.outputanalyzer-importer-ph div').appendChild(this.importer.e)

    this.getElement('.outputanalyzer-importer-ph input[type="checkbox"]')
      .addEventListener('click', e => {
        this.importer.setDirectoryMode(e.target.checked)
    })

    this.buttons = []

    this.noOutputBox = this.getElement('.no-output-box')

    this.outputContentBoxes = this.getElements('.output-file-content')

    this.dosBsBox = this.getElement('.dos-bs-box')
		this.bzViewerBox = this.getElement('.bz-viewer-box')
    this.bsErrorMsgBox = this.getElement('.bs-error-msg')

    this.inputFilesBox = this.getElement('.input-files-section')
  }


  /**
   * Updates the component for a code change (application level event)
   * Do nothing in this case
   * @param  {string} code
   */
  updateForCode(code){
    //log('updateForCode')
  }


  /**
   * Updates the component for a setting change (application level event)
   * Do nothing in this case
   * @param  {object} newSettings
   */
  updateSettings(newSettings){
  }


  /**
   * Module initilization method.
   * Sets the behavior of the module when the calculation output files
   * are imported: they are parsed and the relevant info is shown
   */
  init(){

    this.importer.setImportListener( async files => {

      let promises = []

      // client processing
      for (let i = 0; i < files.length; i++) {
        let file = files[i]
        let reader = new FileReader()
        // when the file is read it triggers the onload event above.
        reader.readAsText(file)

        promises.push(new Promise( (resolve, reject) => {
          reader.addEventListener('load', e => {
            resolve({ content: e.target.result, name: file.name })
          })
        }))
      }

      Promise.all(promises).then(

        (filesData) => {
          // reset the panels
          showElement(this.noOutputBox, false)
          showElement(this.outputContentBoxes[0], false)
          showElement(this.dosBsBox, false)
          showElement(this.outputContentBoxes[1], false)
          showElement(this.inputFilesBox, false)
					showElement(this.bzViewerBox, false)

          let output = getOutputInstance(filesData)
          //console.log('output', output);

          if (!output){
            this.noOutputBox.innerHTML = NO_FILE_TEXT
            showElement(this.noOutputBox)
            return
          }

          // Desambiguation modal window. More than one output file and/or two DOS data files
          const outputFiles = output.files.output
          const dosFiles = output.files.dos
          if (outputFiles.size > 1 || dosFiles.size > 1){
            // FileChooser popup appears
            let fileChooser = new FileChooser(outputFiles, dosFiles)
            // this happens when the FileChooser button is clicked
            fileChooser.setListener( (outputFileName, dosFileName) => {
              //log('outputFileName, dosFile', outputFileName, dosFile)
              if (outputFiles.size > 0){
                // If only one output file: outputFileName = undefined, no choice
								try {
									output.parseOutputFile(outputFileName)
								} catch (e) {
									this.noOutputBox.innerHTML = PARSING_ERROR
									showElement(this.noOutputBox)
								}
                 // The first is parsed
                this.showOutputLayout(output)
              }else {
                this.noOutputBox.innerHTML = NO_MAIN_FILE_TEXT
                showElement(this.noOutputBox)
              }

              // If only one dos file: dosFileName = undefined, no choice
              if (!dosFileName && dosFiles.size === 1)
                dosFileName = dosFiles.keys().next().value // The first is gotten
              let bsInfo = output.getBsInfo()
              this.showBSAndDOSData(bsInfo.bsData, dosFiles.get(dosFileName), bsInfo.lackingSegments,output.getStructure())

              ModalPopup.hideModal()
            })
            ModalPopup.setModalComponent(fileChooser.e)
            ModalPopup.showModal(true)

          }else{ // No modal window: no more than one output file and DOS data file

						if (outputFiles.size === 1) { // there is output file
							try {
								output.parseOutputFile()
							} catch(e) {
								this.noOutputBox.innerHTML = PARSING_ERROR
								showElement(this.noOutputBox)
							}

							this.showOutputLayout(output)
						} else {
							 this.noOutputBox.innerHTML = NO_MAIN_FILE_TEXT
               showElement(this.noOutputBox)
						}

            let dosData = output.getFirstFileDosData() // It can be none
            const bsInfo = output.getBsInfo()
            this.showBSAndDOSData(bsInfo.bsData, dosData, bsInfo.lackingSegments,output.getStructure())
          } // else

        },

        error => {
          UserMsgBox.setMsg('Error uploading files')
          console.error('ERROR ', error)
        }
      )

    })

  } // init()


  /**
   * The DOS and band structure plots are built and shown
   * @param  {object} bsData
   * @param  {object} dosData
   * @param  {object} lackingSegments
   *  Info of the bands structure lacking segments
   */
  showBSAndDOSData(bsData, dosData, lackingSegments, structure=undefined){
    // DOS and bandstructure graphs
    const anyDOSBSData = (dosData || bsData.length > 0)
		let isStructure =  (structure && structure.atoms.length > 0)
    this.dosBsBox.style.display = (anyDOSBSData ? 'block' : 'none')
		this.bzViewerBox.style.display = ( ( isStructure && (bsData.length > 0)) ? 'flex' : 'none')
    if (anyDOSBSData){
      if (this.dosBsGraphs === undefined){
        this.dosBsGraphs = new DOSBSGraphs()
        this.dosBsBox.querySelector('.section-content').appendChild(this.dosBsGraphs.e)
      }
      this.dosBsGraphs.setData(dosData, bsData)

      if (bsData.length > 0 && lackingSegments.length > 0)
        this.bsErrorMsgBox.innerHTML = 'Some band structure files are lacking: '+lackingSegments.join(', ')
      else
        this.bsErrorMsgBox.innerHTML = ''
    }
		let table = this.getElement('.band-path-table')
		if (table)
			this.bzViewerBox.removeChild(table)
		// console.log(isStructure > 0, bsData , bsData.length > 0, lackingSegments);
		if (isStructure > 0 && bsData && bsData.length > 0) {
			if (this.bzViewer === undefined)
				this.bzViewer = new BZViewer(this.getElement('.bz-viewer-ph'))
			let paths = this.bzViewer.getBandPath(bsData)
			if (paths) {
				this.bzViewer.setNewStructure(structure,true,paths=paths)
				let table = this.bzViewer.getHTMLBandPath(paths)
				this.bzViewerBox.appendChild(table)
			}
		} else {
			this.bzViewerBox.style.display = 'none'
		}
  }


  /**
   * The main output information is shown
   * @param  {object} output
   */
  showOutputLayout(output){

    showElement(this.outputContentBoxes[0])
    showElement(this.outputContentBoxes[1])

    const isRelaxation = output.isRelaxation()
		let structure = output.getStructure()

    //  **** System information  section ****

    this.getElement('.formula-value').textContent = output.systemInformation.formulaUnit
    this.getElement('.atom-num-value').textContent = output.systemInformation.nAtoms

    // Structure viewer (animation for relaxation case)
    if (this.viewer) this.viewer.reset()
    else this.viewer = new StructureViewer(this.getElement('.geometry-viz-ph'))
		// console.log(output.scfLoops);
    const structureSequence = []
    if (isRelaxation) {
			// console.log(output.calculation.scfLoops);
      let i = 0
      output.scfLoops.forEach( scfLoop => {
        // log('scfLoop.structure', i++, scfLoop.structure)
        if (scfLoop.structure) {
          // let loopStructure = new Structure()
          // loopStructure.atoms = scfLoop.structure.atoms
          //***** TODO lattice vector??
          structureSequence.push(scfLoop.structure)
        }
      })
			// console.log(structureSequence);
      if (structureSequence.length > 0)
        this.viewer.setNewAnimation(structureSequence)
    }else{
      this.viewer.setNewStructure(structure)
    }

    //  **** Results section ****
    this.getElement('.results-info-fields').innerHTML = getHtmlRows(output.getResultsQuantities())

    //  ***** Summary calculation section

    // Calculation information
		this.getElement('.calculation-info-fields').innerHTML = getHtmlRows(output.getCalculationInfo(), false)
    // Relaxation graph
    if (isRelaxation) { // Print this graph only if relaxation
      if (!this.relaxationGraph){
        this.relaxationGraph = new RelaxationGraph()
        this.getElement('.relaxation-graph-ph').appendChild(this.relaxationGraph.e)
      }
      this.relaxationGraph.setData(output.relaxationSeries)
    }
    this.getElement('.relaxation-graph-ph').style.display = (isRelaxation ? 'block' : 'none')

    // Convergence graph
    if (!this.convergenceGraph){
      this.convergenceGraph = new ConvergenceGraph()
      this.getElement('.convergence-graph-ph').appendChild(this.convergenceGraph.e)
    }
    this.convergenceGraph.setData(output.dataSeries)

    //**** Input files section ****

    let inputFileMap = output.getInputFilesMap()

    showElement(this.inputFilesBox, inputFileMap.size > 0)
    this.getElement('.input-files-box').innerHTML = ''

    inputFileMap.forEach( (content, name) => {
      const href = window.URL.createObjectURL( new Blob([content], {type: 'text/plain'}) )
      const boxElement = document.createElement("div")
      boxElement.innerHTML = `
          <button class="input-files-button"> View <b>${name}</b> </button>
          <a class="download-link" download="${name}" href="${href}">
            <img style="vertical-align: middle; padding-left: 10px" src="${downloadIcon}" />
          </a>
        `
      let dtext = content.replace(/[\u00A0-\u9999<>\&]/gim, function(i) {
        return '&#'+i.charCodeAt(0)+';';
      })

      boxElement.querySelectorAll('.input-files-button').forEach( e => {
        e.addEventListener('click', e => {
          ModalPopup.setModalContent('<blockquote><pre><code>'+dtext+'</code></pre></blockquote>')
          ModalPopup.showModal(true)
        })
      })

      this.getElement('.input-files-box').appendChild(boxElement)
    })


    // Parsing errors - by Sebastian
		// console.log(output.errors);
		this.getElement('.parsing-files-section').style.display = (output.errors.length > 0 ? 'block' : 'none')
		let html = ''
		output.errors.forEach(er => {
			console.log(er);
			html += `<tr> <td>${er[0]}</td> <td>${er[1].replace(/[\u00A0-\u9999<>\&]/gim, function(i) {
        return '&#'+i.charCodeAt(0)+';';
      })}</td> </tr>`
		})
		this.getElement('.parsing-info-fields').innerHTML = html
  }

}



/**
 * Simple UI component that enables the choice between two files of the same type
 * (output and DOS files). It's shown inside a modal popup
 */
class FileChooser extends UIComponent{

  /**
   * @param  {Map<string,string>}
   * @param  {Map<string,string>}
   */
  constructor(outputFiles, dosFiles){

    super('div', '.extensionChooser')
    let html = ''

    if (outputFiles.size > 1){
      html += '<div>More than one aims output file uploaded, choose the one to be displayed:</div><div class="radios-ph">'
      outputFiles.forEach( (data, fileName) => {
        html += `<input type="radio" name="output-file-name" value="${fileName}"> ${fileName} <br>`
      })
      html += '</div>'
    }

    if (dosFiles.size > 1){
      html += '<div>Two DOS data files uploaded, choose the one to be displayed:</div><div class="radios-ph">'
      dosFiles.forEach( (data, fileName) => {
        html += `<input type="radio" name="dos-data-file" value="${fileName}"> ${fileName} <br>`
      })
      html += '</div>'
    }

    this.setHTML(html+'<div class="button-ph"><button>Done</button></div>')

    this.getElement('button').addEventListener( 'click', buttonEvent => {
      //log('DOS file'+chosenInput.value)
      let outputFileEl = this.getElement('input[name="output-file-name"]:checked')
      let dosFileEl = this.getElement('input[name="dos-data-file"]:checked')
      this.listener( (outputFileEl === null ? undefined : outputFileEl.value), (dosFileEl === null ? undefined : dosFileEl.value))
    })
  }


  /**
   * Sets the listener that will be called when the button is clicked
   * @param {function} listener
   */
  setListener(listener){
    this.listener = listener
  }

}



const ENERGY_AXIS_MAX = 20
const ENERGY_AXIS_MIN = -20
const BS_X_AXIS_LABEL = 'Band path'
const DOS_X_AXIS_LABEL = 'DOS (states/eV/cell)'
const Y_AXIS_LABEL = 'Energy (eV)'
const DEFAULT_COLOR = '#000000'
const SPIN1_DEFAULT_COLOR = '#f26430'
const SPIN2_DEFAULT_COLOR = '#009ddc'

const DEFAULT_LABELS_FONT_MULTIPLIER = 1.4
const DEFAULT_TICKS_FONT_MULTIPLIER = 1.2
const DEFAULT_LINES_THICKNESS_MULTIPLIER = 2

/**
 * UI component hosting the DS and band structure plots.
 * Adds several controls enabling the modification of the plots style
 */
class DOSBSGraphs extends UIComponent{

  constructor(){
    super('div', '.DOSBSGraphs')
    this.setHTML(`
      <div id="band-structure-graph" > </div>
      <div id="dos-graph" > </div>
      <div class="graph-controls-box" >
        <div>
          <div style="text-align: center">
            <img class="home-button" style="vertical-align: middle; text-align: center" src="${refreshIcon}" height="20px">
          </div>
          <!-- Here it goes the form -->
        </div>
      </div>
    `)

    this.width = 900
    this.height = 500
    this.dosPlotter
    this.bsPlotter

    this.getElement('.home-button').addEventListener( 'click', e => {
      this.resetFieldsAndPlots()
    })

    // Build the form

    this.form = new Form('OutputAnalizer-graph-controls-form')
    this.form.disableUserMsg()

    this.form.addSection('Labels style')
    this.axesLabelsSizeField = this.form.addField('axesLabelsSize', 'Axes label size', 'input-range', [1, 2.2, DEFAULT_LABELS_FONT_MULTIPLIER])
    this.labelColorField = this.form.addField('labelsColor', 'Axes label color', 'input-color', DEFAULT_COLOR)
    this.tickLabelSizeField = this.form.addField('axesValuesSize', 'Tick label size', 'input-range', [1, 2, DEFAULT_TICKS_FONT_MULTIPLIER])

    this.form.addSection('Data lines style')
    //this.linesGroupsField = this.form.addField('linesGroups', 'Spin lines', 'input-checkbox:2:spin1,spin2')
    this.linesThicknessField = this.form.addField('linesThickness', 'Thickness', 'input-range', [1, 6, DEFAULT_LINES_THICKNESS_MULTIPLIER])
    this.linesColorSpin1Field = this.form.addField('linesColorSpin1', 'Spin1 color', 'input-color', SPIN1_DEFAULT_COLOR)
    this.linesColorSpin2Field = this.form.addField('linesColorSpin2', 'Spin2 color', 'input-color', SPIN2_DEFAULT_COLOR)


    let sectionBox = this.form.addSection('Band structure &nbsp;&nbsp;')
    this.bsScreenshotTaker = new ScreenshotTaker('bandstructure', '', 15)
    this.bsScreenshotTaker.setScreenshotListener( linkElement => {
      linkElement.href = this.bsPlotter.baseGraph.canvas.toDataURL("image/png", 1)
    })
    this.bsScreenshotTaker.e.style.display = 'inline'
    sectionBox.classList.add('bsDiv')
    sectionBox.appendChild(this.bsScreenshotTaker.e)
    this.bsXAxisLabelField = this.form.addField('bsXAxisLabel', 'X axis label', 'input-text', BS_X_AXIS_LABEL)
    this.bsXAxisLabelField.parentElement.parentElement.classList.add('bsDiv')
    this.bsYAxisLabelField = this.form.addField('bsYAxisLabel', 'Y axis label', 'input-text', Y_AXIS_LABEL)
    this.bsYAxisLabelField.parentElement.parentElement.classList.add('bsDiv')

    sectionBox =this.form.addSection('DOS &nbsp;&nbsp;')
    sectionBox.classList.add('dosDiv')
    this.dosScreenshotTaker = new ScreenshotTaker('DOS', '', 15)
    this.dosScreenshotTaker.setScreenshotListener( linkElement => {
      linkElement.href = this.dosPlotter.baseGraph.canvas.toDataURL("image/png", 1)
    })
    this.dosScreenshotTaker.e.style.display = 'inline'
    sectionBox.appendChild(this.dosScreenshotTaker.e)
    this.dosXAxisLabelField = this.form.addField('dosXAxisLabel', 'X axis label', 'input-text', DOS_X_AXIS_LABEL)
    this.dosXAxisLabelField.parentElement.parentElement.classList.add('dosDiv')
    this.dosYAxisLabelField = this.form.addField('dosYAxisLabel', 'Y axis label', 'input-text', Y_AXIS_LABEL)
    this.dosYAxisLabelField.parentElement.parentElement.classList.add('dosDiv')

    this.getElement('.graph-controls-box div').appendChild(this.form.e)

    this.axesLabelsSizeField.addEventListener('input', e => { // change
      const multiplier = parseFloat(e.target.value)
      this.changeLabelsAndTitleStyle(multiplier, undefined)
    })
    this.labelColorField.addEventListener('input', e => {
      this.changeLabelsAndTitleStyle(undefined, e.target.value)
    })
    this.tickLabelSizeField.addEventListener('input', e => { // change
      const multiplier = parseFloat(e.target.value)
      this.changeTickLabelsStyle(multiplier, undefined, true)
    })
    this.linesThicknessField.addEventListener('input', e => {
      const multiplier = parseFloat(e.target.value)
      this.changeDataLinesStyle(multiplier, undefined, 'spin1')
      this.changeDataLinesStyle(multiplier, undefined, 'spin2')
    })
    this.linesColorSpin1Field.addEventListener('input', e => {
      this.changeDataLinesStyle(undefined, e.target.value, 'spin1')
    })
    this.linesColorSpin2Field.addEventListener('input', e => {
      this.changeDataLinesStyle(undefined, e.target.value, 'spin2')
    })

    this.bsXAxisLabelField.addEventListener('input', e => {
      this.bsPlotter.baseGraph.changeLabelsText(e.target.value, undefined)
    })
    this.bsYAxisLabelField.addEventListener('input', e => {
      this.bsPlotter.baseGraph.changeLabelsText(undefined, e.target.value)
    })
    this.dosXAxisLabelField.addEventListener('input', e => {
      this.dosPlotter.baseGraph.changeLabelsText(e.target.value, undefined)
    })
    this.dosYAxisLabelField.addEventListener('input', e => {
      this.dosPlotter.baseGraph.changeLabelsText(undefined, e.target.value)
    })
  }


  /**
   * Sets the DOS and band structure data to be drawn
   * @param {object} dosData
   * @param {object} bsData
   */
  setData(dosData, bsData){

    if (this.dosPlotter) {
      this.dosPlotter.baseGraph.parentElement.style.display = (dosData ? 'block' : 'none')
    }
    this.form.e.querySelectorAll('.dosDiv').forEach( e => {
      e.style.display = (dosData ? 'flex' : 'none')
    })

    if (dosData){
      if (!this.dosPlotter){
        this.dosPlotter = new DOSPlotter({left: 180, right: 50, top: 50, bottom: 120}, DOS_X_AXIS_LABEL, Y_AXIS_LABEL,
          SPIN1_DEFAULT_COLOR, SPIN2_DEFAULT_COLOR)
        this.dosPlotter.attach(this.getElement('#dos-graph'), this.width*0.3, this.height, true)
      }
      this.dosPlotter.setData(dosData, ENERGY_AXIS_MIN, ENERGY_AXIS_MAX)
    }

    if (this.bsPlotter) {
      this.bsPlotter.baseGraph.parentElement.style.display = (bsData.length > 0 ? 'block' : 'none')
      this.form.e.querySelectorAll('.bsDiv').forEach( e => {
        e.style.display = (bsData.length > 0 ? 'flex' : 'none')
      })
    }

    if (bsData.length > 0){
      if (!this.bsPlotter){
        this.bsPlotter = new BSPlotter({left: 180, right: 50, top: 50, bottom: 120}, BS_X_AXIS_LABEL, Y_AXIS_LABEL,
          SPIN1_DEFAULT_COLOR, SPIN2_DEFAULT_COLOR)
        this.bsPlotter.attach(this.getElement('#band-structure-graph'), this.width*0.7, this.height, true)
      }

      this.bsPlotter.setBandStructureData(bsData, ENERGY_AXIS_MIN, ENERGY_AXIS_MAX)

    }

    if (dosData && bsData.length > 0){
      this.dosPlotter.setRepaintListener( (yZoom, yOffset) => {
        this.bsPlotter.baseGraph.setYZoomAndOffset(yZoom, yOffset, true)
      })
      this.bsPlotter.setRepaintListener( (yZoom, yOffset) => {
        this.dosPlotter.baseGraph.setYZoomAndOffset(yZoom, yOffset, true)
      })
    }
    // New redraw
    this.resetFieldsAndPlots()
  }


  /**
   * Reset the control fields and the plots
   */
  resetFieldsAndPlots(){
    const LINES_THICKNESS = {groups: ['spin1', 'spin2'], thicknessMult: DEFAULT_LINES_THICKNESS_MULTIPLIER}
    if (this.bsPlotter) this.bsPlotter.baseGraph.setDefaultStyleAndZoom(DEFAULT_LABELS_FONT_MULTIPLIER, DEFAULT_TICKS_FONT_MULTIPLIER, LINES_THICKNESS)
    if (this.dosPlotter) this.dosPlotter.baseGraph.setDefaultStyleAndZoom(DEFAULT_LABELS_FONT_MULTIPLIER, DEFAULT_TICKS_FONT_MULTIPLIER, LINES_THICKNESS)
    this.form.resetFieldsValues()
  }


  /**
   * Changes the axes labels and the title styles
   * @param  {float} sizeMult Font size multiplier
   * @param  {string} color Color code
   */
  changeLabelsAndTitleStyle(sizeMult, color){
    if (this.bsPlotter)
      this.bsPlotter.baseGraph.changeLabelsAndTitleStyle(sizeMult, color)
    if (this.dosPlotter)
      this.dosPlotter.baseGraph.changeLabelsAndTitleStyle(sizeMult, color)
  }


  /**
   * Changes the tick labels style
   * @param  {float} sizeMult Font size multiplier
   * @param  {string} color Color code
   * @param {boolean} draw If true the canvas is drawn
   */
  changeTickLabelsStyle(sizeMult, color, draw = false){
    if (this.bsPlotter)
      this.bsPlotter.baseGraph.changeTickLabelsStyle(sizeMult, color, draw)
    if (this.dosPlotter)
      this.dosPlotter.baseGraph.changeTickLabelsStyle(sizeMult, color, draw)
  }


  /**
   * Changes the data lines style
   * @param  {float} thicknessMult Lines thickness multiplier
   * @param  {string} color Color code
   * @param  {string} group Lines group
   */
  changeDataLinesStyle(thicknessMult, color, group){
    if (this.bsPlotter)
      this.bsPlotter.baseGraph.changeDataLinesStyle(thicknessMult, color, group)
    if (this.dosPlotter)
      this.dosPlotter.baseGraph.changeDataLinesStyle(thicknessMult, color, group)
  }

}



/**
 * Calculation convergence graph
 */
class ConvergenceGraph extends UIComponent{

  constructor(){
    super('div')

    this.canvas = document.createElement('canvas')
    this.canvas.className = 'convergence-canvas'
    this.e.appendChild(this.canvas)

    this.graphConfig = {
        "type":"line",
        "options":{
          scales: {
            xAxes: [{display: true, scaleLabel: { display: true, labelString: 'Iteration step'} }],
            yAxes: [{
              display: true,
              type: 'logarithmic',
              ticks: { userCallback: function(tick) {
                var remain = tick / (Math.pow(10, Math.floor(Chart.helpers.log10(tick))));
                if (remain === 1) {
                  return tick.toExponential().toString();
                }
                return '';
              }}
            }]
          },
          layout: {
            padding: { left: 0, right: 0, top: 0, bottom: 0 }
          },
          maintainAspectRatio: true
        }
      }
    //Create chart for numerical convergence.
    this.chart = new Chart.Chart(this.canvas, this.graphConfig)
  }


  /**
   * Sets the data to be drawn
   * @param {object}
   */
  setData(results){

    this.results = results
    let lastIter = results.length-1

    if (this.results.length > 1){

      if (this.stepSelector === undefined){
        this.stepSelector = new PaginationSelector('Relaxation step', 0)
        this.e.insertBefore(this.stepSelector.e, this.canvas)

        this.stepSelector.setPrevListener( s => {
          this.setIterData(s)
        })

        this.stepSelector.setNextListener( s => {
          this.setIterData(s)
        })
      }

      this.stepSelector.init(lastIter+1)
      this.stepSelector.e.style.display = 'block'

    }else if (this.stepSelector !== undefined)
      this.stepSelector.e.style.display = 'none'

    this.setIterData(lastIter)
    if (this.stepSelector !== undefined) this.stepSelector.setPage(lastIter)

  }


  /**
   * Sets and draws the data of an iteration
   * @param {object} iter
   */
  setIterData(iter){
    let iterations = this.results[iter]

    this.chart.data.datasets = []

    const addDataSet = (data, label, color) => {
      this.chart.data.datasets.push({
        "label": label,
        "data": data.map(Math.abs),
        "fill": false,
        "borderColor": color,
        "lineTension": 0.1
      })
    }
		let numberIterations = 0
		for (let [key,entry] of Object.entries(iterations)) {
			addDataSet(entry.data, entry.label,entry.color)
			numberIterations = entry.data.length
		}

		let steps = []
		for (let i = 0; i < numberIterations; i++) steps.push(i+1)
		this.chart.data.labels = steps

    this.chart.update()
  }

}



/**
 * Calculation relaxation graph
 */
class RelaxationGraph extends UIComponent{

  constructor(){
    super('div')
    this.canvas = document.createElement('canvas')
    this.canvas.className = 'relaxation-canvas'
    this.e.appendChild(this.canvas)

    this.configI = {
      "type":"line",
      "options":{
        scales: {
          xAxes: [{ display: true, scaleLabel: { display: true, labelString: 'Relaxation step'} }],
          yAxes: [{
              display: true,
							type: 'logarithmic',
              id: 'ymaxforce',
              position: 'right',
              scaleLabel: { display: true, labelString: 'Force (eV/A)' }
            },
            {
              display: true,
							type: 'logarithmic',
              id: 'yscenergy',
							position: 'left',
              scaleLabel: { display: true, labelString: 'Total Energy Difference (eV)' }
            }]
        }
      }
    }
  }


  /**
   * Sets the data to be drawn
   * @param {object}
   */
  setData(results){
    this.configI.data = {datasets:[]}
		this.configI.data.labels = results.labels
		this.configI.data.datasets.push(results.energy)
		this.configI.data.datasets.push(results.forces)
		this.chart = new Chart.Chart(this.canvas, this.configI)
  }

}
