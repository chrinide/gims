### Sixth release (*r6*)

*(From November 18 to January xx, 2020)*

.... 

Main r6 features:
- **Exciting code support** (major application refactoring and new functionality):
  - Code selector IU component and corresponding new application state and logic
  - Update of the import/export functionality (*StructureBuilder* module)
  - Control generator module adaptation (new form fields and server request parametars)
  - Workflow adaptation (code election disabled inside, workflow configuration improvement, etc)
- **Advanced interactivity in graphs**:
  - Structure Builder: possibility of color change of the showing species (by clicking at the species circles on the legend)
  - Band structure and DOS plots: 
    - Possibility of taking screenshots of the plots
    - The plot labels are now editable and adjustable in size and color
    - Axes ticks labels are adjustable in size
    - The lines (by spin) are adjustable in color and thickness 
    - Button that returns to the original state of the plot
    - Zooming improvements

More work done this time:
  - New own 2D graphic library (`Canvas.js` file)
  - *InfoCanvas* and DOS/BS plots refactoring (on the new library) and performance optimization. 
  - Color picker integration (first use: species color change - Structure builder)
  - Application refactoring (images imports) and application resources review and cleaning

### Fifth release (*r5*)

*(From September 19 to November 15, 2019)*

The main feature in this release is the addition of a Band Structure and DOS workflow and their visualization possibility on an improved *Output Analyzer*. On the other hand, we have found, tested and implemented a way to bundle and distribute the application as a desktop/offline app. 

Main r5 features:
- **Band Structure and DOS data visualization** integrated in the *Output Analyzer*:
  - *Output Analyzer* file import logic and multi-file support (folder source support).
  - Bands and DOS files data parsing and flexible graphical representation
- New **Band Structure & DOS workflow** and improved *Control Generator*:
  - New *Band Structure & DOS workflow*
  - Improved *Control Generator*: More flexible and supporting sections (Accordion user interface) 
  - Backend based *control.in* generation (use the *control.in* writer from ASE). (by Sebastian)
- Application **packaging and distribution as a desktop/offline** app research and first implementation:
  - Research into the *PyInstaller* tool and decision of adoption
  - Testing on Linux and MacOS, optimization and documentation
  - The desktop application (versions for Linux and MacOS) is already downloadable (via the web application)
- Top level/dashboard restructuring:
  - Change of workflows, layout and buttons (designed by Sebastian)
 
More work done this time: 
  - **Error handling** through the application. Refactoring and documentation ([Application error handling](Application-error-handling) doc on the Wiki)
  - Structure builder improvements (by Sebastian)
    - Simple supercell handling at the right panel of the structure builder UI
    - Constrain atoms via a checkbox at the basis atoms panel. The material of the corresponding atom changes. 
    - The distance between atoms is checked. If they become too close, then, the bond color changes to red and the bond radius is increased.
  - User feedback UI component at application level. A way to give simple feedback to the user (errors, warnings and informative tips). It's a text box at the top-center of application layout 
  - Meaningful modification of FileImporter component
  - Simpler console logging/error call (Global utility functions)
  


### Fourth release (*r4*)

*(From August 5 to September 18, 2019)*

This time we kept expanding the application: a new module *Control generator* has been added, as well as a new important application feature: workflows support. 

Main r4 features: 
- ***Control Generator* application module**:
  - It generates *control.in* files from a user form and species defaults files
  - New generic ***Form*** component 
  - URL fragment associated to the module (*#ControlGenerator*)
- **Workflows** support: 
  - UI header (*AssistantHeader*) enables the workflow interaction
  - Declarative (partially) workflow definition
  - Two workflows (similar, only declarative difference) implemented
  - Modules adaptations to be integrated in the workflows  

More work done this time: 
  - ***ASE* library integration** and server request based geometry importer (*Structure Builder* module)
  - Import **geometry file format resolution** user interface 
  - Some work on the adaptation and integration of the *DOS* and *Band Structure* graphs (from the *NOMAD* project).
  - Some refactoring (asynchronous programming based since now on *promises* and *async/await*) and documentation (Workflows feature)
  

### Third release (*r3*)

*(From May 2 to July 26, 2019)*

From this version, the application is more than a *Structure builder*. This becomes a module in the app and a new one is added: the *Output Analyzer*.

- Improvements on the previous (r2b) release: 
  - Supercell construction based on a supercell matrix (based on Sebastian’s code) 
  - Refactoring: ES6 import/export and dependency reduction on the math.js library (goal: get rid of it) 
  - New Switch UI component (used for Atom displacement) 
  - Add a checkbox to the lattice vector section to enable/disable the scaling of atom positions as lattice vectors change 

Main r3 features: 
  - **Non-periodic systems** support (Structure Builder module): file parsing, Structure viewer and UI adaptation 
    - Lattice vectors can be removed or created at any moment and circumstance 
    - The lattice vectors creation/removals are supported by the undo/redo system 
  - Top level **application module: Dashboard**. 
  - Application **Output Analyzer module**: 
    - Output file(s) importer: simple and relaxation calculation output support (integration of Sebastian’s file parsing code) 
    - Module page layout (divided in sections) 
    - Integration of the StructureViewer and development of structure animations support 
    - 2D charts integration and enhancement from Sebastian’s code 
    - Input files: geometry.in and control.in popup viewer and downloader 
    - URL fragment associated to the module (#OutputAnalyzer) 

More work done this time: 
  - Refactor the **StructureViewer as an app library** (reusable application module) 
  - Create a changelog document (Gitlab wiki) 
  - Research and new doc (Gitlab wiki): **Web client architecture and technical decisions** 
  - Modal popup component 
  - Some research and discussion about the next big goal: the first complete workflow 


## Structure builder 

### Second release - second part (*r2b*)

*(From March 5 to April 29, 2019)*

- Improvements on the previous release:

  - In an atom displacement, new bonds are calculated and automatically updated on the viewer and the new atom coordinates are shown (atom info area) at mouse release time
  - Fixed performance penalty issue (mathjs library methods) related to bonds calculation
  - Screenshot feature improvement: take a screenshot in one click
  - Unify and make well-proportioned the arrows of the lattice vectors and labels for any cell size. 
  - The corresponding atom is highlighted when the mouse pointer hovers the atom row (right panel)

Main features:

- **Measurements** of distance between atoms (2 atoms selected), angle (3 atoms selected) and torsion angle (4 atoms selected). The info is shown on the top-right part of the viewer.

- Possibility of creating **supercells** from the current cell. URL fragments interface: this funtion is launched by adding a fragment to the main URL. e.g. `URL#repeat:2:2:3`

- **Undo/redo feature**: system for going back and forth in the history of changes (both via UI and key combination Ctrl+z Ctrl+y). Changes supported: atom creation, move, species change, removal, lattice vectors modification and supercell transformation.

More work done:

- Important refactoring. The `Reactjs` library was removed from the project and implemented a new client architecture. 

- **Transfer the application to the FHI infrastructure** 

- **Performance and memory optimization** research, testing and implementation of measures

- We started the **project documentation** on gitlab (wiki section) with three pages: Development roadmap, Performance optimization and Setup WSGI for gui on apache2. We intent to document every meaningful aspect of the project from now

- **Atom selection mechanism** (multi-selection enabled). Now there are 3 atom interaction ways: hovering, selection and drag&drop. Selection: Shift key + mouse click. Clear selection: mouse right-click

- Formalization of the URL fragment (browser feature) use (starting with the hash symbol - #). For now only param repeat supported


### Second release - first part (*r2a*)

*(From early January to March 1, 2019)*

- Improvements of the previous release:
  - Bonds representation improvement (algorithm to identify bonds across cell boundaries)
  - UI element to switch between fractional and cartesian coordinates
  - More digits (5) in the coordinate fields. Several solutions were considered and visually tested
  - More options (left-top checkboxes) added to the 3D viewer:
    - Option of switching on/off the lattice vectors
    - Option of switching on/off the unit cell box
    - Wrap atoms into cell (If atoms are outside the box, move them to the equivalent positions inside the box)
    - Option of switching on/off the bonds across the cell boundaries

Main features:

- Structure **creation from scratch**

- **Manipulation of imported** (from files) structures. You can (editing text fields):
   - edit the lattice vectors (modify the cell) and the atoms coordinates (move the atoms)
   - change the species (substitution of atoms)
   - add and remove atoms

- **Enabled interaction** with the structure elements **on the 3D viewer**. You can:
   - Move atoms dragging them (mouse click and release)
   - Show textual info (on the viewer) of the atom hovered by the mouse pointer
   - Atom highlights (in 3D view) by hovering the circle in the atom row (side panel)

- **Export** of the editor current state of the structure in 'geometry.in' format (both in fractional and cartesian coordinates)

More work included in this release:

- **Screenshot capture** functionality

- Moving atoms with the mouse enabled via a checkbox (to avoid accidents)

- Side panel **new button bar** (*Import - Export - Reset*) and user interaction

- Default structure when the web app is loaded

### First release (r1)

*(From mid November to the end of December, 2018)*

Main features:

- **Work environment set-up**: `Reactjs` (frontend library) + `Flask` (python backend) + `GoogleAppEngine` (cloud hosting)

- **Structure 3D viewer**: based on `Threejs` library (using *WebGL* browser capability)
  - Basic cell, atoms and lattice vectors and parameters representation
  - Interaction: zoom and rotation
  - Bonds and atoms on cell boundaries
  - Legend and structure visualization control

- **Side panel** showing lattice vectors and atoms alphanumeric data

- **Import of existing structures** feature. Formats supported: 
  - FHI-aims format ‘geometry.in’
  - Crystallographic Information File (CIF) 
